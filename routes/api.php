<?php

use App\Http\Controllers\CarouselController;
use App\Http\Controllers\Enums;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\ShippingDataController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->post('/storeUser', [RegisterController::class, 'store']);
Route::middleware('auth:sanctum')->post('/checkEmailAvailability', [RegisterController::class, 'checkEmailAvailability']);
Route::middleware('auth:sanctum')->post('/checkUsernameAvailability', [RegisterController::class, 'checkUsernameAvailability']);
Route::middleware('auth:sanctum')->get('/confirmUser/{id}', [RegisterController::class, 'confirmViaId']);
Route::middleware(['signed', 'throttle:6,1'])->get('/email/verify/{id}/{hash}', [RegisterController::class, 'verifyViaHash'])->name('verification.verify');
Route::middleware('auth:sanctum')->get('/resendVerification/{id}', [RegisterController::class, 'resendVerification']);
Route::get('/setVerificationAsShown/{id}', [RegisterController::class, 'setVerificationAsShown']);

Route::post('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout']);
Route::get('/checkAuth', [LoginController::class, 'checkAuth']);
Route::get('/checkAuth/{role}', [LoginController::class, 'checkAuth']);

Route::middleware('auth:sanctum')->post('/storeItem', [ItemController::class, 'store']);
Route::middleware('auth:sanctum')->post('/editItem', [ItemController::class, 'edit']);
Route::middleware('auth:sanctum')->get('/markItem/{id}', [ItemController::class, 'mark']);
Route::middleware('auth:sanctum')->get('/deleteItem/{id}', [ItemController::class, 'delete']);
Route::get('/getItem/{id}', [ItemController::class, 'getItem']);
Route::get('/getItems', [ItemController::class, 'getItems']);
Route::post('/getCartItems', [ItemController::class, 'getCartItems']);

Route::post('/sendMessage', [MessageController::class, 'store']);
Route::middleware('auth:sanctum')->get('/getMessages', [MessageController::class, 'getMessages']);
Route::middleware('auth:sanctum')->get('/getMessage/{id}', [MessageController::class, 'getMessage']);
Route::middleware('auth:sanctum')->post('/replyOnMessage', [MessageController::class, 'reply']);
Route::middleware('auth:sanctum')->post('/resendMail', [MessageController::class, 'resend']);

Route::middleware('auth:sanctum')->post('/storeService', [ServiceController::class, 'store']);
Route::middleware('auth:sanctum')->post('/editService', [ServiceController::class, 'edit']);
Route::middleware('auth:sanctum')->get('/markService/{id}', [ServiceController::class, 'mark']);
Route::middleware('auth:sanctum')->get('/deleteService/{id}', [ServiceController::class, 'delete']);
Route::get('/getService/{id}', [ServiceController::class, 'getService']);
Route::get('/getServices', [ServiceController::class, 'getServices']);

Route::middleware('auth:sanctum')->post('/storeOffer', [OfferController::class, 'store']);
Route::middleware('auth:sanctum')->post('/editOffer', [OfferController::class, 'edit']);
Route::middleware('auth:sanctum')->get('/markOffer/{id}', [OfferController::class, 'mark']);
Route::middleware('auth:sanctum')->get('/deleteOffer/{id}', [OfferController::class, 'delete']);
Route::get('/getOffer/{id}', [OfferController::class, 'getOffer']);
Route::get('/getOffers', [OfferController::class, 'getOffers']);

Route::get('/getCategories', [Enums::class, 'getCategories']);
Route::get('/getConditions', [Enums::class, 'getConditions']);
Route::get('/getMessageOptions', [Enums::class, 'getMessageOptions']);
Route::get('/getServiceTypes', [Enums::class, 'getServiceTypes']);
Route::get('/getBrands', [Enums::class, 'getBrands']);
Route::get('/getAllEnums', [Enums::class, 'getAllEnums']);
Route::get('/getLanguages', [Enums::class, 'getLanguages']);
Route::get('/getGenders', [Enums::class, 'getGenders']);
Route::middleware('auth:sanctum')->get('/getOrderStatus', [Enums::class, 'getOrderStatus']);
Route::get('/getDeliveryTypes', [Enums::class, 'getDeliveryTypes']);

Route::middleware('auth:sanctum')->post('/newPass', [UserController::class, 'changePass']);
Route::middleware('auth:sanctum')->post('/changeLanguage', [UserController::class, 'changeLanguage']);
Route::middleware('auth:sanctum')->post('/checkPasswordMatch', [UserController::class, 'checkPasswordMatch']);
Route::middleware('auth:sanctum')->get('/getRoles', [UserController::class, 'getRoles']);
Route::middleware('auth:sanctum')->get('/getAllRoles', [UserController::class, 'getAllRoles']);
Route::middleware('auth:sanctum')->post('/editUser', [UserController::class, 'edit']);
Route::middleware('auth:sanctum')->get('/markUser/{id}', [UserController::class, 'block']);
Route::middleware('auth:sanctum')->get('/getUser/{id}', [UserController::class, 'getUser']);
Route::middleware('auth:sanctum')->get('/getUsers', [UserController::class, 'getUsers']);
Route::middleware('auth:sanctum')->get('/switchShowInactive', [UserController::class, 'switchShowInactive']);
Route::middleware('auth:sanctum')->get('/switchShowDeleted', [UserController::class, 'switchShowDeleted']);
Route::get('/checkUser/{userId}', [UserController::class, 'isVerificationViewShown']);

Route::middleware('auth:sanctum')->post('/storeCarouselItem', [CarouselController::class, 'store']);
Route::middleware('auth:sanctum')->post('/editCarouselItem', [CarouselController::class, 'edit']);
Route::middleware('auth:sanctum')->get('/markCarouselItem/{id}', [CarouselController::class, 'mark']);
Route::middleware('auth:sanctum')->get('/deleteCarouselItem/{id}', [CarouselController::class, 'delete']);
Route::get('/getCarouselItem/{id}', [CarouselController::class, 'getItem']);
Route::get('/getCarouselItems', [CarouselController::class, 'getItems']);

Route::post('/makeOrder', [OrderController::class, 'makeOrder']);
Route::get('/checkout/success', [OrderController::class, 'success']);
Route::get('/checkout/cancel', [OrderController::class, 'cancel']);
Route::post('/webhook', [OrderController::class, 'webhook']);
Route::get('/getSimpleOrderBySessionId/{sessionId}', [OrderController::class, 'getSimpleOrderBySessionId']);
Route::get('/markOrder/{id}', [OrderController::class, 'mark']);
Route::middleware('auth:sanctum')->get('/getOrder/{id}', [OrderController::class, 'getOrder']);
Route::middleware('auth:sanctum')->get('/getOrders', [OrderController::class, 'getOrders']);
Route::middleware('auth:sanctum')->post('/setOrderStatus', [OrderController::class, 'setOrderStatus']);
Route::middleware('auth:sanctum')->post('/updateOrderLastOpenedAt', [OrderController::class, 'updateLastOpenedAt']);

Route::middleware('auth:sanctum')->post('/createOrUpdateShippingData', [ShippingDataController::class, 'storeOrEdit']);
Route::middleware('auth:sanctum')->get('/markShippingData/{id}', [ShippingDataController::class, 'mark']);
Route::middleware('auth:sanctum')->get('/getShippingData/{id}', [ShippingDataController::class, 'getShippingData']);
Route::get('/getShippingDataList', [ShippingDataController::class, 'getShippingDataList']);
