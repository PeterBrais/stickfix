<?php

namespace Tests\Unit;

use App\Enums\Language;
use App\Enums\ServiceType;
use App\Models\Service;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

/**
 * Summary of ServiceTest
 */
class ServiceTest extends TestCase
{
    /**
     * Summary of testClearServiceTableAtBeginning
     * @return void
     */
    public function testClearServiceTableAtBeginning()
    {
        if (config('app.env') === 'testing') {
            Service::truncate();
        }

        $this->assertDatabaseCount('services', 0);
    }

    /**
     * Summary of testStoreServiceAsUnauthenticated
     * @return void
     */
    public function testStoreServiceAsUnauthenticated()
    {
        $serviceData = [
            'serviceType' => ServiceType::OFFER,
            'title' => 'Him boisterous invitation',
            'note' => 'im boisterous invitation dispatched had connection inhabiting projection.',
            'language' => Language::EN,
            'price' => 5.00,
            'isActive' => 1,
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeService', $serviceData, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $storedServiceData = [
            'serviceType' => $serviceData['serviceType'],
            'title' => $serviceData['title'],
            'note' => $serviceData['note'],
            'language' => $serviceData['language'],
            'priceInMinorUnit' => $serviceData['price'] * 100,
            'isActive' => $serviceData['isActive'],
        ];

        $this->assertDatabaseMissing('services', $storedServiceData);
    }

    /**
     * Summary of testStoreServiceAsAuthenticated
     * @return void
     */
    public function testStoreServiceAsAuthenticated()
    {
        $userData = [
            'username' => 'aida_weissnat97',
            'email' => 'aida_weissnat97@gmail.com',
            'password' => 'aida_weissnat97123',
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $serviceData = [
            'serviceType' => ServiceType::BUY,
            'title' => 'Sitting mistake towards his few country ask',
            'note' => 'Sitting mistake towards his few country ask. You delighted two rapturous six depending objection happiness something the.',
            'language' => Language::EN,
            'price' => 4.00,
            'isActive' => 1,
            'recaptcha' => 'testing',
        ];

        $storedServiceData = [
            'serviceType' => $serviceData['serviceType'],
            'title' => $serviceData['title'],
            'note' => $serviceData['note'],
            'language' => $serviceData['language'],
            'priceInMinorUnit' => $serviceData['price'] * 100,
            'isActive' => $serviceData['isActive'],
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('POST', 'api/storeService', $serviceData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
        $this->assertDatabaseHas('services', $storedServiceData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        Service::truncate();
        $this->assertDatabaseMissing('services', $storedServiceData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testEditServiceAsUnauthenticated
     * @return void
     */
    public function testEditServiceAsUnauthenticated()
    {
        $serviceData = [
            'serviceType' => ServiceType::BUY,
            'title' => 'Forfeited you engrossed',
            'note' => 'conduct at an replied removal an amongst. Remaining determine few her two cordially admitting old.',
            'language' => Language::EN,
            'priceInMinorUnit' => 400,
            'isActive' => 1,
        ];

        $storedServiceData = [
            'serviceType' => $serviceData['serviceType'],
            'title' => $serviceData['title'],
            'note' => $serviceData['note'],
            'language' => $serviceData['language'],
            'priceInMinorUnit' => $serviceData['priceInMinorUnit'],
            'isActive' => $serviceData['isActive'],
        ];

        $serviceObj = Service::factory()->create($serviceData);
        $this->assertDatabaseHas('services', $storedServiceData);

        $editedServiceData = [
            'id' => $serviceObj->id,
            'serviceType' => ServiceType::OFFER,
            'title' => 'Advice me cousin an sprin',
            'note' => 'Tiled man stand tears ten joy there terms any widen. Procuring continued suspicion its ten.',
            'language' => null,
            'price' => 8.00,
            'isActive' => 1,
            'recaptcha' => 'testing',
        ];

        $storedEditedServiceData = [
            'serviceType' => $editedServiceData['serviceType'],
            'title' => $editedServiceData['title'],
            'note' => $editedServiceData['note'],
            'language' => $editedServiceData['language'],
            'priceInMinorUnit' => $editedServiceData['price'] * 100,
            'isActive' => $editedServiceData['isActive'],
        ];

        $this->json('POST', 'api/editService', $editedServiceData, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $this->assertDatabaseHas('services', $storedServiceData);
        $this->assertDatabaseMissing('services', $storedEditedServiceData);

        $serviceObj->delete();
        $this->assertDatabaseMissing('services', $storedServiceData);
    }

    /**
     * Summary of testEditServiceAsAuthenticated
     * @return void
     */
    public function testEditServiceAsAuthenticated()
    {
        $userData = [
            'username' => 'cindy45',
            'email' => 'cindy45@hotmail.com',
            'password' => 'cindy45123',
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $serviceData = [
            'serviceType' => ServiceType::OFFER,
            'title' => 'Up is opinion message',
            'note' => 'Up is opinion message manners correct hearing husband my. Disposing commanded dashwoods cordially depending at at',
            'language' => Language::EN,
            'priceInMinorUnit' => 800,
            'isActive' => 1,
        ];

        $storedServiceData = [
            'serviceType' => $serviceData['serviceType'],
            'title' => $serviceData['title'],
            'note' => $serviceData['note'],
            'language' => $serviceData['language'],
            'priceInMinorUnit' => $serviceData['priceInMinorUnit'],
            'isActive' => $serviceData['isActive'],
        ];

        $serviceObj = Service::factory()->create($serviceData);
        $this->assertDatabaseHas('services', $storedServiceData);

        $editedServiceData = [
            'id' => $serviceObj->id,
            'serviceType' => ServiceType::BUY,
            'title' => 'Its strangers who you certainty',
            'note' => '',
            'language' => null,
            'price' => 9.00,
            'isActive' => 0,
            'recaptcha' => 'testing',
        ];

        $storedEditedServiceData = [
            'serviceType' => $editedServiceData['serviceType'],
            'title' => $editedServiceData['title'],
            'note' => null,
            'language' => $editedServiceData['language'],
            'priceInMinorUnit' => $editedServiceData['price'] * 100,
            'isActive' => $editedServiceData['isActive'],
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('POST', 'api/editService', $editedServiceData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $this->assertDatabaseHas('services', $storedEditedServiceData);
        $this->assertDatabaseMissing('services', $storedServiceData);

        $serviceObj->delete();
        $this->assertDatabaseMissing('services', $storedEditedServiceData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testMarkServiceAsUnauthenticated
     * @return void
     */
    public function testMarkServiceAsUnauthenticated()
    {
        $serviceData = [
            'serviceType' => ServiceType::OFFER,
            'title' => 'By so delight of showing neither',
            'note' => 'Deal sigh up in shew away when. Pursuit express no or prepare replied',
            'language' => Language::EN,
            'priceInMinorUnit' => 1000,
            'isActive' => 1,
        ];

        $storedServiceData = [
            'serviceType' => $serviceData['serviceType'],
            'title' => $serviceData['title'],
            'note' => $serviceData['note'],
            'language' => $serviceData['language'],
            'priceInMinorUnit' => $serviceData['priceInMinorUnit'],
            'isActive' => $serviceData['isActive'],
        ];

        $serviceObj = Service::factory()->create($serviceData);
        $this->assertDatabaseHas('services', $storedServiceData);

        $this->json('GET', 'api/markService/' . $serviceObj->id, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $serviceObj->delete();
        $this->assertDatabaseMissing('services', $storedServiceData);
    }

    /**
     * Summary of testMarkServiceAsAuthenticated
     * @return void
     */
    public function testMarkServiceAsAuthenticated()
    {
        $userData = [
            'username' => 'kristofer.dietrich',
            'email' => 'kristofer.dietrich@yahoo.com',
            'password' => Hash::make('kristofer.dietrich123'),
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $serviceData = [
            'serviceType' => ServiceType::BUY,
            'title' => 'When be draw drew ye',
            'note' => 'Defective in do recommend suffering.',
            'language' => Language::EN,
            'priceInMinorUnit' => 999,
            'isActive' => true,
        ];

        $storedServiceData = [
            'serviceType' => $serviceData['serviceType'],
            'title' => $serviceData['title'],
            'note' => $serviceData['note'],
            'language' => $serviceData['language'],
            'priceInMinorUnit' => $serviceData['priceInMinorUnit'],
            'isActive' => $serviceData['isActive'],
        ];

        $serviceObj = Service::factory()->create($serviceData);
        $this->assertDatabaseHas('services', $storedServiceData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/markService/' . $serviceObj->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('services', $storedServiceData);

        $storedServiceData['isActive'] = false;
        $this->assertDatabaseHas('services', $storedServiceData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $serviceObj->delete();
        $this->assertDatabaseMissing('services', $storedServiceData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testDeleteServiceAsUnauthenticated
     * @return void
     */
    public function testDeleteServiceAsUnauthenticated()
    {
        $serviceData = [
            'serviceType' => ServiceType::OFFER,
            'title' => 'no purse as fully me or point',
            'note' => 'indness own whatever betrayed her moreover procured replying for and',
            'language' => Language::LV,
            'priceInMinorUnit' => 1200,
            'isActive' => 1,
        ];

        $storedServiceData = [
            'serviceType' => $serviceData['serviceType'],
            'title' => $serviceData['title'],
            'note' => $serviceData['note'],
            'language' => $serviceData['language'],
            'priceInMinorUnit' => $serviceData['priceInMinorUnit'],
            'isActive' => $serviceData['isActive'],
        ];

        $serviceObj = Service::factory()->create($serviceData);
        $this->assertDatabaseHas('services', $storedServiceData);

        $this->json('GET', 'api/deleteService/' . $serviceObj->id, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $serviceObj->delete();
        $this->assertDatabaseMissing('services', $storedServiceData);
    }

    /**
     * Summary of testDeleteServiceAsAuthenticated
     * @return void
     */
    public function testDeleteServiceAsAuthenticated()
    {
        $userData = [
            'username' => 'adele.gibson28',
            'email' => 'adele.gibson28@hotmail.com',
            'password' => Hash::make('adele.gibson28123'),
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $serviceData = [
            'serviceType' => ServiceType::BUY,
            'title' => 'indulged no do',
            'note' => 'Covered ten nor comfort offices carried',
            'language' => Language::LV,
            'priceInMinorUnit' => 1500,
            'isActive' => 1,
        ];

        $storedServiceData = [
            'serviceType' => $serviceData['serviceType'],
            'title' => $serviceData['title'],
            'note' => $serviceData['note'],
            'language' => $serviceData['language'],
            'priceInMinorUnit' => $serviceData['priceInMinorUnit'],
            'isActive' => $serviceData['isActive'],
        ];

        $serviceObj = Service::factory()->create($serviceData);
        $this->assertDatabaseHas('services', $storedServiceData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/deleteService/' . $serviceObj->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('services', $storedServiceData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testGetServiceById
     * @return void
     */
    public function testGetServiceById()
    {
        $serviceData = [
            'serviceType' => ServiceType::OFFER,
            'title' => 'Of incommode supported provision',
            'note' => 'on furnished objection exquisite me',
            'language' => Language::LV,
            'priceInMinorUnit' => 2000,
            'isActive' => 1,
        ];

        $storedServiceData = [
            'serviceType' => $serviceData['serviceType'],
            'title' => $serviceData['title'],
            'note' => $serviceData['note'],
            'language' => $serviceData['language'],
            'priceInMinorUnit' => $serviceData['priceInMinorUnit'],
            'isActive' => $serviceData['isActive'],
        ];

        $serviceObj = Service::factory()->create($serviceData);
        $this->assertDatabaseHas('services', $storedServiceData);

        $this->json('GET', 'api/getService/' . $serviceObj->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'service' => [ 'id' => $serviceObj->id ],
            ]);

        $serviceObj->delete();
        $this->assertDatabaseMissing('services', $storedServiceData);
    }

    /**
     * Summary of testGetServices
     * @return void
     */
    public function testGetServices()
    {
        $firstServiceData = [
            'serviceType' => ServiceType::BUY,
            'title' => 'Seen you eyes son show',
            'note' => 'Far two unaffected one alteration apartments',
            'language' => Language::EN,
            'priceInMinorUnit' => 2500,
            'isActive' => 1,
        ];

        $firstStoredServiceData = [
            'serviceType' => $firstServiceData['serviceType'],
            'title' => $firstServiceData['title'],
            'note' => $firstServiceData['note'],
            'language' => $firstServiceData['language'],
            'priceInMinorUnit' => $firstServiceData['priceInMinorUnit'],
            'isActive' => $firstServiceData['isActive'],
        ];

        $firstServiceObj = Service::factory()->create($firstServiceData);
        $this->assertDatabaseHas('services', $firstStoredServiceData);

        $secondServiceData = [
            'serviceType' => ServiceType::BUY,
            'title' => 'Seen you eyes son show',
            'note' => 'Far two unaffected one alteration apartments',
            'language' => null,
            'priceInMinorUnit' => 3000,
            'isActive' => 1,
        ];

        $secondStoredServiceData = [
            'serviceType' => $secondServiceData['serviceType'],
            'title' => $secondServiceData['title'],
            'note' => $secondServiceData['note'],
            'priceInMinorUnit' => $secondServiceData['priceInMinorUnit'],
            'isActive' => $secondServiceData['isActive'],
        ];

        $secondServiceObj = Service::factory()->create($secondServiceData);
        $this->assertDatabaseHas('services', $secondStoredServiceData);

        $this->json('GET', 'api/getServices', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'services' => [
                    [ 'id' => $firstServiceObj->id ],
                    [ 'id' => $secondServiceObj->id ],
                ],
            ]);

        $firstServiceObj->delete();
        $this->assertDatabaseMissing('services', $firstStoredServiceData);
        $secondServiceObj->delete();
        $this->assertDatabaseMissing('services', $secondStoredServiceData);
    }

    /**
     * Summary of testClearServiceTableAtEnd
     * @return void
     */
    public function testClearServiceTableAtEnd()
    {
        if (config('app.env') === 'testing') {
            Service::truncate();
        }

        $this->assertDatabaseCount('services', 0);
    }
}
