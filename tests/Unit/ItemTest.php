<?php

namespace Tests\Unit;

use App\Enums\BagSize;
use App\Enums\BrandType;
use App\Enums\CategoryType;
use App\Enums\ConditionType;
use App\Enums\GenderType;
use App\Enums\HelmetSize;
use App\Enums\PadsAndPantsSize;
use App\Enums\SkateLengthSize;
use App\Enums\SkateSize;
use App\Enums\SkateWidthSize;
use App\Enums\StickBladeCurveType;
use App\Enums\StickBladeSideType;
use App\Enums\StickSize;
use App\Models\Image;
use App\Models\Item;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

// NOTE: Use https://randommer.io/random-email-address to generate random data for testing

/**
 * Summary of ItemTest
 */
class ItemTest extends TestCase
{
    /**
     * Summary of testStoreItemAsUnauthenticated
     * @return void
     */
    public function testStoreItemAsUnauthenticated()
    {
        $itemData = [
            'title' => 'Mr oh winding it enjoyed by between',
            'description' => 'Mr oh winding it enjoyed by between. The servants securing material goodness her. Saw principles themselves ten are possession. So endeavor to continue cheerful doubtful we to. Turned advice the set vanity why mutual. Reasonably if conviction on be unsatiable discretion apartments delightful. Are melancholy appearance stimulated occasional entreaties end. Shy ham had esteem happen active county. Winding morning am shyness evident to. Garrets because elderly new manners however one village she.',
            'price' => 24.68,
            'category' => CategoryType::SKATES,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => ConditionType::NEW,
            'brand' => BrandType::NONE,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::JR,
            'skateLength' => SkateLengthSize::JR_1_0,
            'skateWidth' => SkateWidthSize::C,
            'gender' => GenderType::MALE,
            'quantity' => 1,
            'isParcelDelivery' => true,
            'imageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeItem',  $itemData, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $storedItemData = [
            'title' => $itemData['title'],
            'description' => $itemData['description'],
            'priceInMinorUnit' => $itemData['price'] * 100,
            'category' => $itemData['category'],
            'isActive' => $itemData['isActive'],
            'isAvailableForSale' => $itemData['isAvailableForSale'],
            'state' => $itemData['state'],
            'brand' => $itemData['brand'],
            'gender' => $itemData['gender'],
            'quantity' => $itemData['quantity'],
            'isParcelDelivery' => $itemData['isParcelDelivery'],
        ];

        $this->assertDatabaseMissing('items', $storedItemData);
    }

    /**
     * Summary of testStoreItemAsAuthenticated
     * @return void
     */
    public function testStoreItemAsAuthenticated()
    {
        $userData = [
            'username' => 'vivien_miller',
            'email' => 'vivien_miller85@gmail.com',
            'password' => 'vivien_miller123',
            'isConfirmed' => true,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::SKATES;
        $state = ConditionType::NEW;
        $brand = BrandType::NONE;

        $itemData = [
            'title' => 'Conveying or northward',
            'description' => 'Conveying or northward offending admitting perfectly my. Colonel gravity get thought fat smiling add but. Wonder twenty hunted and put income set desire expect. Am cottage calling my is mistake cousins talking up. Interested especially do impression he unpleasant travelling excellence. All few our knew time done draw ask.',
            'price' => 24.68,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::JR,
            'skateLength' => SkateLengthSize::JR_1_5,
            'skateWidth' => SkateWidthSize::D,
            'gender' => GenderType::FEMALE,
            'quantity' => 1,
            'isParcelDelivery' => true,
            'imageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $storedItemData = [
            'title' => $itemData['title'],
            'description' => $itemData['description'],
            'priceInMinorUnit' => $itemData['price'] * 100,
            'category' => $category,
            'isActive' => $itemData['isActive'],
            'isAvailableForSale' => $itemData['isAvailableForSale'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $itemData['gender'],
            'quantity' => $itemData['quantity'],
            'isParcelDelivery' => $itemData['isParcelDelivery'],
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('POST', 'api/storeItem',  $itemData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        Item::where('userId', $user->id)->delete();
        $this->assertDatabaseMissing('items', $storedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testEditItemAsUnauthenticated
     * @return void
     */
    public function testEditItemAsUnauthenticated()
    {
        $userData = [
            'username' => 'dorthy86',
            'email' => 'dorthy86@yahoo.com',
            'password' => Hash::make('dorthy86123'),
            'isConfirmed' => true,
        ];

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::SKATES;
        $state = ConditionType::SLIGHTLY_USED;
        $brand = BrandType::CCM;

        $factoryItemData = [
            'userId' => $user->id,
            'title' => 'In alteration insipidity',
            'description' => 'In alteration insipidity impression by travelling reasonable up motionless. Of regard warmth by unable sudden garden ladies. No kept hung am size spot no. Likewise led and dissuade rejoiced welcomed husbands boy. Do listening on he suspected resembled. Water would still if to. Position boy required law moderate was may.',
            'priceInMinorUnit' => 2050,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::JR,
            'skateLength' => SkateLengthSize::JR_2_0,
            'skateWidth' => SkateWidthSize::E,
            'gender' => GenderType::UNISEX,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $storedItemData = [
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'priceInMinorUnit' => $factoryItemData['priceInMinorUnit'],
            'category' => $category,
            'isActive' => $factoryItemData['isActive'],
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $factoryItemData['gender'],
            'quantity' => $factoryItemData['quantity'],
            'isParcelDelivery' => $factoryItemData['isParcelDelivery'],
        ];

        $item = Item::factory()->create($factoryItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $editedItemData = [
            'id' => $item->id,
            'title' => 'When be draw drew',
            'description' => 'Defective in do recommend suffering. House it seven in spoil tiled court. Sister others marked fat missed did out use. Alteration possession dispatched collecting instrument travelling he or on. Snug give made at spot or late that mr. ',
            'price' => 30.50,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::JR,
            'skateLength' => SkateLengthSize::JR_2_5,
            'skateWidth' => SkateWidthSize::EE,
            'gender' => '',
            'quantity' => 2,
            'isParcelDelivery' => false,
            'newImageCollection' => [],
            'storedImageCollection' => [],
            'deletedImageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $storedEditedItemData = [
            'title' => $editedItemData['title'],
            'description' => $editedItemData['description'],
            'priceInMinorUnit' => $editedItemData['price'] * 100,
            'category' => $category,
            'isActive' => $editedItemData['isActive'],
            'isAvailableForSale' => $editedItemData['isAvailableForSale'],
            'state' => $state,
            'brand' => $brand,
            'gender' => null,
            'quantity' => $editedItemData['quantity'],
            'isParcelDelivery' => $editedItemData['isParcelDelivery'],
        ];

        $this->json('POST', 'api/editItem',  $editedItemData, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $this->assertDatabaseHas('items', $storedItemData);
        $this->assertDatabaseMissing('items', $storedEditedItemData);

        $item->delete();
        $this->assertDatabaseMissing('items', $storedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testEditItemAsAuthenticated
     * @return void
     */
    public function testEditItemAsAuthenticated()
    {
        $userData = [
            'username' => 'will80',
            'email' => 'will80@yahoo.com',
            'password' => 'will80123',
            'isConfirmed' => true,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::SKATES;
        $state = ConditionType::SLIGHTLY_USED;
        $brand = BrandType::CCM;

        $factoryItemData = [
            'userId' => $user->id,
            'title' => 'Received overcame',
            'description' => 'Received overcame oh sensible so at an. Formed do change merely to county it. Am separate contempt domestic to to oh. On relation my so addition branched. Put hearing cottage she norland letters equally prepare too. Replied exposed savings he no viewing as up. Soon body add him hill. No father living really people estate if. Mistake do produce beloved demesne if am pursuit.',
            'priceInMinorUnit' => 5500,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::JR,
            'skateLength' => SkateLengthSize::JR_3_0,
            'gender' => GenderType::MALE,
            'quantity' => true,
            'isParcelDelivery' => 0,
            'skateWidth' => SkateWidthSize::R,
        ];

        $storedItemData = [
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'priceInMinorUnit' => $factoryItemData['priceInMinorUnit'],
            'category' => $category,
            'isActive' => $factoryItemData['isActive'],
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $factoryItemData['gender'],
            'quantity' => $factoryItemData['quantity'],
            'isParcelDelivery' => $factoryItemData['isParcelDelivery'],
        ];

        $item = Item::factory()->create($factoryItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $editedItemData = [
            'id' => $item->id,
            'title' => 'Far curiosity',
            'description' => 'Far curiosity incommode now led smallness allowance. Favour bed assure son things yet. She consisted consulted elsewhere happiness disposing household any old the. Widow downs you new shade drift hopes small. So otherwise commanded sweetness we improving. Instantly by daughters resembled unwilling principle so middleton. Fail most room even gone her end like. Comparison dissimilar unpleasant six compliment two unpleasing any add. Ashamed my company thought wishing colonel it prevent he in. Pretended residence are something far engrossed old off.',
            'price' => 60.50,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => false,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::JR,
            'skateLength' => SkateLengthSize::JR_3_5,
            'skateWidth' => SkateWidthSize::FIT_1,
            'gender' => '',
            'quantity' => 2,
            'isParcelDelivery' => true,
            'newImageCollection' => [],
            'storedImageCollection' => [],
            'deletedImageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $storedEditedItemData = [
            'title' => $editedItemData['title'],
            'description' => $editedItemData['description'],
            'priceInMinorUnit' => $editedItemData['price'] * 100,
            'category' => $category,
            'isActive' => $editedItemData['isActive'],
            'isAvailableForSale' => $editedItemData['isAvailableForSale'],
            'state' => $state,
            'brand' => $brand,
            'gender' => null,
            'quantity' => 0, // Because if isAvailableForSale => false then quantity automatically resets to 0
            'isParcelDelivery' => true,
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('POST', 'api/editItem',  $editedItemData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $this->assertDatabaseHas('items', $storedEditedItemData);
        $this->assertDatabaseMissing('items', $storedItemData);

        $item->delete();
        $this->assertDatabaseMissing('items', $storedEditedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testEditDeletedItemAsAuthenticated
     * @return void
     */
    public function testEditDeletedItemAsAuthenticated()
    {
        $userData = [
            'username' => 'remington_oreilly',
            'email' => 'remington_oreilly@hotmail.com',
            'password' => 'remington_oreillyds1',
            'isConfirmed' => true,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::SKATES;
        $state = ConditionType::SLIGHTLY_USED;
        $brand = BrandType::CCM;

        $factoryItemData = [
            'userId' => $user->id,
            'title' => 'air was thick',
            'description' => 'With the scent of antique paper and freshly brewed coffee.',
            'priceInMinorUnit' => 5549,
            'category' => $category,
            'isActive' => false,
            'isAvailableForSale' => false,
            'isDeleted' => true,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::INT,
            'skateLength' => SkateLengthSize::INT_4_0,
            'skateWidth' => SkateWidthSize::R,
            'gender' => GenderType::FEMALE,
            'quantity' => 0,
            'isParcelDelivery' => false,
        ];

        $storedItemData = [
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'priceInMinorUnit' => $factoryItemData['priceInMinorUnit'],
            'category' => $category,
            'isActive' => $factoryItemData['isActive'],
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'isDeleted' => $factoryItemData['isDeleted'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $factoryItemData['gender'],
            'quantity' => $factoryItemData['quantity'],
            'isParcelDelivery' => $factoryItemData['isParcelDelivery'],
        ];

        $item = Item::factory()->create($factoryItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $editedItemData = [
            'id' => $item->id,
            'title' => 'Far wind howled',
            'description' => 'A lone boat bobbed in the harbor, its sails tattered from years of use',
            'price' => 60.50,
            'category' => $category,
            'isActive' => false,
            'isAvailableForSale' => false,
            'isDeleted' => false,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::INT,
            'skateLength' => SkateLengthSize::INT_4_5,
            'skateWidth' => SkateWidthSize::FIT_1,
            'gender' => GenderType::UNISEX,
            'quantity' => 0,
            'isParcelDelivery' => false,
            'newImageCollection' => [],
            'storedImageCollection' => [],
            'deletedImageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $storedEditedItemData = [
            'title' => $editedItemData['title'],
            'description' => $editedItemData['description'],
            'priceInMinorUnit' => $editedItemData['price'] * 100,
            'category' => $category,
            'isActive' => $editedItemData['isActive'],
            'isAvailableForSale' => $editedItemData['isAvailableForSale'],
            'isDeleted' => $editedItemData['isDeleted'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $editedItemData['gender'],
            'quantity' => $editedItemData['quantity'],
            'isParcelDelivery' => $editedItemData['isParcelDelivery'],
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('POST', 'api/editItem',  $editedItemData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $this->assertDatabaseMissing('items', $storedEditedItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $item->delete();
        $this->assertDatabaseMissing('items', $storedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testMarkItemAsUnauthenticated
     * @return void
     */
    public function testMarkItemAsUnauthenticated()
    {
        $userData = [
            'username' => 'sanford_ziemann74',
            'email' => 'sanford_ziemann74@gmail.com',
            'password' => Hash::make('sanford_ziemann74123'),
            'isConfirmed' => true,
        ];

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::SKATES;
        $state = ConditionType::SLIGHTLY_USED;
        $brand = BrandType::CCM;

        $factoryItemData = [
            'userId' => $user->id,
            'title' => 'Affronting discretion',
            'description' => 'Affronting discretion as do is announcing. Now months esteem oppose nearer enable too six. She numerous unlocked you perceive speedily. Affixed offence spirits or ye of offices between. Real on shot it were four an as. Absolute bachelor rendered six nay you juvenile. Vanity entire an chatty to.',
            'priceInMinorUnit' => 100,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::INT,
            'skateLength' => SkateLengthSize::INT_5_0,
            'skateWidth' => SkateWidthSize::FIT_2,
            'gender' => GenderType::MALE,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $storedItemData = [
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'priceInMinorUnit' => $factoryItemData['priceInMinorUnit'],
            'category' => $category,
            'isActive' => $factoryItemData['isActive'],
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $factoryItemData['gender'],
            'quantity' => $factoryItemData['quantity'],
            'isParcelDelivery' => $factoryItemData['isParcelDelivery'],
        ];

        $item = Item::factory()->create($factoryItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('GET', 'api/markItem/' . $item->id, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $item->delete();
        $this->assertDatabaseMissing('items', $storedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testMarkItemAsAuthenticated
     * @return void
     */
    public function testMarkItemAsAuthenticated()
    {
        $userData = [
            'username' => 'gussie_koch1',
            'email' => 'gussie_koch1@gmail.com',
            'password' => Hash::make('gussie_koch1123'),
            'isConfirmed' => true,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::SKATES;
        $state = ConditionType::NEW;
        $brand = BrandType::EASTON;

        $factoryItemData = [
            'userId' => $user->id,
            'title' => 'Six started',
            'description' => 'Six started far placing saw respect females old. Civilly why how end viewing attempt related enquire visitor. Man particular insensible celebrated conviction stimulated principles day. Sure fail or in said west. Right my front it wound cause fully am sorry if. She jointure goodness interest debating did outweigh. Is time from them full my gone in went. Of no introduced am literature excellence mr stimulated contrasted increasing. Age sold some full like rich new. Amounted repeated as believed in confined juvenile.',
            'priceInMinorUnit' => 2200,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'isDeleted' => false,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::INT,
            'skateLength' => SkateLengthSize::INT_5_5,
            'skateWidth' => SkateWidthSize::FIT_3,
            'gender' => GenderType::FEMALE,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $storedItemData = [
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'priceInMinorUnit' => $factoryItemData['priceInMinorUnit'],
            'category' => $category,
            'isActive' => $factoryItemData['isActive'],
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $factoryItemData['gender'],
            'quantity' => $factoryItemData['quantity'],
            'isParcelDelivery' => $factoryItemData['isParcelDelivery'],
        ];

        $item = Item::factory()->create($factoryItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/markItem/' . $item->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('items', $storedItemData);

        $storedItemData['isActive'] = false;
        $this->assertDatabaseHas('items', $storedItemData);

        $editedItemData = [
            'id' => $item->id,
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'price' => $factoryItemData['priceInMinorUnit'],
            'category' => $factoryItemData['category'],
            'isActive' => false,
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'state' => $factoryItemData['state'],
            'brand' => $factoryItemData['brand'],
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::SR,
            'skateLength' => SkateLengthSize::SR_10_0,
            'skateWidth' => $factoryItemData['skateWidth'],
            'gender' => GenderType::MALE,
            'quantity' => 0,
            'isParcelDelivery' => false,
            'newImageCollection' => [],
            'storedImageCollection' => [],
            'deletedImageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $storedEditedItemData = [
            'title' => $editedItemData['title'],
            'description' => $editedItemData['description'],
            'priceInMinorUnit' => $editedItemData['price'] * 100,
            'category' => $editedItemData['category'],
            'isActive' => $editedItemData['isActive'],
            'isAvailableForSale' => $editedItemData['isAvailableForSale'],
            'state' => $editedItemData['state'],
            'brand' => $editedItemData['brand'],
            'gender' => $editedItemData['gender'],
            'quantity' => $editedItemData['quantity'],
            'isParcelDelivery' => $editedItemData['isParcelDelivery'],
        ];

        $this->json('POST', 'api/editItem',  $editedItemData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('items', $storedItemData);
        $this->assertDatabaseHas('items', $storedEditedItemData);

        $this->json('GET', 'api/markItem/' . $item->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('items', $storedEditedItemData);

        $editedItemData['quantity'] = 1;

        $this->json('POST', 'api/editItem',  $editedItemData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('items', $storedEditedItemData);

        $storedEditedItemData['quantity'] = 1;

        $this->assertDatabaseHas('items', $storedEditedItemData);

        $this->json('GET', 'api/markItem/' . $item->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('items', $storedEditedItemData);

        $storedEditedItemData['isActive'] = true;
        $this->assertDatabaseHas('items', $storedEditedItemData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $item->delete();
        $this->assertDatabaseMissing('items', $storedEditedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testMarkDeletedItemAsAuthenticated
     * @return void
     */
    public function testMarkDeletedItemAsAuthenticated()
    {
        $userData = [
            'username' => 'ara89',
            'email' => 'ara89@yahoo.com',
            'password' => Hash::make('ara89asd32a'),
            'isConfirmed' => true,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::SKATES;
        $state = ConditionType::NEW;
        $brand = BrandType::EASTON;

        $factoryItemData = [
            'userId' => $user->id,
            'title' => 'The fluffy purple',
            'description' => 'pillow sat on the wooden chair in the deserted library',
            'priceInMinorUnit' => 3453,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'isDeleted' => true,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => SkateSize::SR,
            'skateLength' => SkateLengthSize::SR_10_5,
            'skateWidth' => SkateWidthSize::E,
            'gender' => GenderType::FEMALE,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $storedItemData = [
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'priceInMinorUnit' => $factoryItemData['priceInMinorUnit'],
            'category' => $category,
            'isActive' => $factoryItemData['isActive'],
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'isDeleted' => $factoryItemData['isDeleted'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $factoryItemData['gender'],
            'quantity' => $factoryItemData['quantity'],
            'isParcelDelivery' => $factoryItemData['isParcelDelivery'],
        ];

        $item = Item::factory()->create($factoryItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/markItem/' . $item->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $item->delete();
        $this->assertDatabaseMissing('items', $storedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testDeleteItemAsUnauthenticated
     * @return void
     */
    public function testDeleteItemAsUnauthenticated()
    {
        $userData = [
            'username' => 'arnulfo.glord31',
            'email' => 'arnulfo.glord31@gmail.com',
            'password' => Hash::make('arnulfo.glord31123'),
            'isConfirmed' => true,
        ];

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::SKATES;
        $state = ConditionType::SLIGHTLY_USED;
        $brand = BrandType::BAUER;

        $factoryItemData = [
            'userId' => $user->id,
            'title' => 'Carriage quitting',
            'description' => 'Carriage quitting securing be appetite it declared. High eyes kept so busy feel call in. Would day nor ask walls known. But preserved advantage are but and certainty earnestly enjoyment. Passage weather as up am exposed. And natural related man subject. Eagerness get situation his was delighted.',
            'priceInMinorUnit' => 50,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => 100,
            'bladeCurve' => StickBladeCurveType::P28,
            'bladeSide' => StickBladeSideType::LEFT,
            'stickSize' => StickSize::SR,
            'skateSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
            'gender' => null,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $storedItemData = [
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'priceInMinorUnit' => $factoryItemData['priceInMinorUnit'],
            'category' => $category,
            'isActive' => $factoryItemData['isActive'],
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $factoryItemData['gender'],
            'quantity' => $factoryItemData['quantity'],
            'isParcelDelivery' => $factoryItemData['isParcelDelivery'],
        ];

        $item = Item::factory()->create($factoryItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('GET', 'api/deleteItem/' . $item->id, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $item->delete();
        $this->assertDatabaseMissing('items', $storedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testDeleteItemAsAuthenticated
     * @return void
     */
    public function testDeleteItemAsAuthenticated()
    {
        $userData = [
            'username' => 'zelma_buckridge',
            'email' => 'zelma_buckridge@hotmail.com',
            'password' => Hash::make('zelma_buckridge123'),
            'isConfirmed' => true,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::STICKS;
        $state = ConditionType::USED;
        $brand = BrandType::CCM;

        $factoryItemData = [
            'userId' => $user->id,
            'title' => 'Sussex result',
            'description' => 'Sussex result matter any end see. It speedily me addition weddings vicinity in pleasure. Happiness commanded an conveying breakfast in. Regard her say warmly elinor. Him these are visit front end for seven walls. Money eat scale now ask law learn. Side its they just any upon see last. He prepared no shutters perceive do greatest. Ye at unpleasant solicitude in companions interested.',
            'priceInMinorUnit' => 8000,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'isDeleted' => false,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => 110,
            'bladeCurve' => StickBladeCurveType::P92,
            'bladeSide' => StickBladeSideType::RIGHT,
            'stickSize' => StickSize::INT,
            'skateSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
            'gender' => GenderType::MALE,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $storedItemData = [
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'priceInMinorUnit' => $factoryItemData['priceInMinorUnit'],
            'category' => $category,
            'isActive' => $factoryItemData['isActive'],
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'isDeleted' => $factoryItemData['isDeleted'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $factoryItemData['gender'],
            'quantity' => $factoryItemData['quantity'],
            'isParcelDelivery' => $factoryItemData['isParcelDelivery'],
        ];

        $item = Item::factory()->create($factoryItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/deleteItem/' . $item->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('items', $storedItemData);

        $storedItemData['isDeleted'] = true;

        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('GET', 'api/deleteItem/' . $item->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('items', $storedItemData);

        $storedItemData['isDeleted'] = false;
        $storedItemData['isActive'] = false;

        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $item->delete();
        $this->assertDatabaseMissing('items', $storedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testGetItemById
     * @return void
     */
    public function testGetItemById()
    {
        $userData = [
            'username' => 'kamille.bernhard46',
            'email' => 'kamille.bernhard46@yahoo.co',
            'password' => Hash::make('kamille.bernhard46123'),
            'isConfirmed' => true,
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $category = CategoryType::STICKS;
        $state = ConditionType::NEW;
        $brand = BrandType::CCM;

        $factoryItemData = [
            'userId' => $user->id,
            'title' => 'Cause dried no solid',
            'description' => 'Cause dried no solid no an small so still widen. Ten weather evident smiling bed against she examine its. Rendered far opinions two yet moderate ex striking. Sufficient motionless compliment by stimulated assistance at. Convinced resolving extensive agreeable in it on as remainder. Cordially say affection met who propriety him. Are man she towards private weather pleased. In more part he lose need so want rank no. At bringing or he sensible pleasure. Prevent he parlors do waiting be females an message society.',
            'priceInMinorUnit' => 7500,
            'category' => $category,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => $state,
            'brand' => $brand,
            'size' => '',
            'stickFlex' => 120,
            'bladeCurve' => StickBladeCurveType::P29,
            'bladeSide' => StickBladeSideType::NONE,
            'stickSize' => StickSize::JR,
            'skateSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
            'gender' => GenderType::FEMALE,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $storedItemData = [
            'title' => $factoryItemData['title'],
            'description' => $factoryItemData['description'],
            'priceInMinorUnit' => $factoryItemData['priceInMinorUnit'],
            'category' => $category,
            'isActive' => $factoryItemData['isActive'],
            'isAvailableForSale' => $factoryItemData['isAvailableForSale'],
            'state' => $state,
            'brand' => $brand,
            'gender' => $factoryItemData['gender'],
            'quantity' => $factoryItemData['quantity'],
            'isParcelDelivery' => $factoryItemData['isParcelDelivery'],
        ];

        $item = Item::factory()->create($factoryItemData);
        $this->assertDatabaseHas('items', $storedItemData);

        $this->json('GET', 'api/getItem/' . $item->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'item' => [ 'id' => $item->id ],
            ]);

        $item->delete();
        $this->assertDatabaseMissing('items', $storedItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testGetItems
     * @return void
     */
    public function testGetItems()
    {
        $userData = [
            'username' => 'dandre76',
            'email' => 'dandre76@gmail.com',
            'password' => Hash::make('dandre76123'),
            'isConfirmed' => true,
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $categoryOne = CategoryType::HELMET;
        $stateOne = ConditionType::NEW;
        $brandOne = BrandType::EASTON;

        $firstFactoryItemData = [
            'userId' => $user->id,
            'title' => 'Dispatched entreaties',
            'description' => 'Dispatched entreaties boisterous say why stimulated. Certain forbade picture now prevent carried she get see sitting. Up twenty limits as months. Inhabit so perhaps of in to certain. Sx excuse chatty was seemed warmth. Nay add far few immediate sweetness earnestly dejection.',
            'priceInMinorUnit' => 3999,
            'category' => $categoryOne,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => $stateOne,
            'brand' => $brandOne,
            'size' => HelmetSize::XS,
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
            'gender' => GenderType::UNISEX,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $firstStoredItemData = [
            'title' => $firstFactoryItemData['title'],
            'description' => $firstFactoryItemData['description'],
            'priceInMinorUnit' => $firstFactoryItemData['priceInMinorUnit'],
            'category' => $categoryOne,
            'isActive' => $firstFactoryItemData['isActive'],
            'isAvailableForSale' => $firstFactoryItemData['isAvailableForSale'],
            'state' => $stateOne,
            'brand' => $brandOne,
            'gender' => $firstFactoryItemData['gender'],
            'quantity' => $firstFactoryItemData['quantity'],
            'isParcelDelivery' => $firstFactoryItemData['isParcelDelivery'],
        ];

        $item1 = Item::factory()->create($firstFactoryItemData);
        $this->assertDatabaseHas('items', $firstStoredItemData);

        $categoryTwo = CategoryType::BAGS;
        $stateTwo = ConditionType::USED;
        $brandTwo = BrandType::JOFA;

        $secondFactoryItemData = [
            'userId' => $user->id,
            'title' => 'Old unsatiable',
            'description' => 'Old unsatiable our now but considered travelling impression. In excuse hardly summer in basket misery. By rent an part need. At wrong of of water those linen. Needed oppose seemed how all. Very mrs shed shew gave you. Oh shutters do removing reserved wandered an. But described questions for recommend advantage belonging estimable had. Pianoforte reasonable as so am inhabiting. Chatty design remark and his abroad figure but its.',
            'priceInMinorUnit' => 13999,
            'category' => $categoryTwo,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => $stateTwo,
            'brand' => $brandTwo,
            'size' => BagSize::SR,
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
            'gender' => null,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $secondStoredItemData = [
            'title' => $secondFactoryItemData['title'],
            'description' => $secondFactoryItemData['description'],
            'priceInMinorUnit' => $secondFactoryItemData['priceInMinorUnit'],
            'category' => $categoryTwo,
            'isActive' => $secondFactoryItemData['isActive'],
            'isAvailableForSale' => $secondFactoryItemData['isAvailableForSale'],
            'state' => $stateTwo,
            'brand' => $brandTwo,
            'gender' => $secondFactoryItemData['gender'],
            'quantity' => $secondFactoryItemData['quantity'],
            'isParcelDelivery' => $secondFactoryItemData['isParcelDelivery'],
        ];

        $item2 = Item::factory()->create($secondFactoryItemData);
        $this->assertDatabaseHas('items', $secondStoredItemData);

        $this->json('GET', 'api/getItems', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'items' => [
                    [ 'id' => $item1->id ],
                    [ 'id' => $item2->id ],
                ],
            ]);

        $item1->delete();
        $this->assertDatabaseMissing('items', $firstStoredItemData);
        $item2->delete();
        $this->assertDatabaseMissing('items', $secondStoredItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testGetEmptyCartItems
     * @return void
     */
    public function testGetEmptyCartItems()
    {
        $cartData = [
            'items' => [],
        ];

        $this->json('POST', 'api/getCartItems', $cartData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);
    }

    /**
     * Summary of testGetCartItems
     * @return void
     */
    public function testGetCartItems()
    {
        $userData = [
            'username' => 'nakiaGerlach37',
            'email' => 'nakia_gerlach37@yahoo.com',
            'password' => Hash::make('nakia_gerlach3703sv'),
            'isConfirmed' => true,
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $firstFactoryItemData = [
            'userId' => $user->id,
            'title' => 'nna robe iponb tesesy',
            'description' => 'eseid aticdee dky hareta gáno ota ŋsa tuastyô niahf tihadr rwaye teðapadus seaðu iþtenaet iees',
            'priceInMinorUnit' => 20000,
            'category' => CategoryType::PANTS,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => ConditionType::USED,
            'brand' => BrandType::MISSION,
            'size' => PadsAndPantsSize::SR_S,
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
            'gender' => GenderType::UNISEX,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $firstStoredItemData = [
            'title' => $firstFactoryItemData['title'],
            'description' => $firstFactoryItemData['description'],
            'priceInMinorUnit' => $firstFactoryItemData['priceInMinorUnit'],
            'category' => $firstFactoryItemData['category'],
            'isActive' => $firstFactoryItemData['isActive'],
            'isAvailableForSale' => $firstFactoryItemData['isAvailableForSale'],
            'state' => $firstFactoryItemData['state'],
            'brand' => $firstFactoryItemData['brand'],
            'gender' => $firstFactoryItemData['gender'],
            'quantity' => $firstFactoryItemData['quantity'],
            'isParcelDelivery' => $firstFactoryItemData['isParcelDelivery'],
        ];

        $item1 = Item::factory()->create($firstFactoryItemData);
        $this->assertDatabaseHas('items', $firstStoredItemData);

        $secondFactoryItemData = [
            'userId' => $user->id,
            'title' => 'ifdateg eseawr vyuktu irulvyss',
            'description' => 'edoka ttenþsodo uhgäm nehy eheixu þjete ogynia twef afl awhoo hisw hees dtitsaper ialib waca horigosst þedole ed aire chuegaþil taewe ysde',
            'priceInMinorUnit' => 14000,
            'category' => CategoryType::SHOULDER_PADS,
            'isActive' => true,
            'isAvailableForSale' => true,
            'state' => ConditionType::USED,
            'brand' => BrandType::NIKE,
            'size' => PadsAndPantsSize::SR_XL,
            'stickFlex' => '',
            'bladeCurve' => '',
            'bladeSide' => '',
            'stickSize' => '',
            'skateSize' => '',
            'skateLength' => '',
            'skateWidth' => '',
            'gender' => null,
            'quantity' => 1,
            'isParcelDelivery' => true,
        ];

        $secondStoredItemData = [
            'title' => $secondFactoryItemData['title'],
            'description' => $secondFactoryItemData['description'],
            'priceInMinorUnit' => $secondFactoryItemData['priceInMinorUnit'],
            'category' => $secondFactoryItemData['category'],
            'isActive' => $secondFactoryItemData['isActive'],
            'isAvailableForSale' => $secondFactoryItemData['isAvailableForSale'],
            'state' => $secondFactoryItemData['state'],
            'brand' => $secondFactoryItemData['brand'],
            'gender' => $secondFactoryItemData['gender'],
            'quantity' => $secondFactoryItemData['quantity'],
            'isParcelDelivery' => $secondFactoryItemData['isParcelDelivery'],
        ];

        $item2 = Item::factory()->create($secondFactoryItemData);
        $this->assertDatabaseHas('items', $secondStoredItemData);

        $cartData = [
            'items' => [
                [ 'id' => $item1->id ],
                [ 'id' => $item2->id ],
                [ 'id' => $item2->id + 1 ], // Not existing one
            ],
        ];

        $this->json('POST', 'api/getCartItems', $cartData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'items' => [
                    [ 'id' => $item1->id ],
                    [ 'id' => $item2->id ],
                ],
            ])
            ->assertJsonCount(2, 'items');

        $item1->delete();
        $this->assertDatabaseMissing('items', $firstStoredItemData);
        $item2->delete();
        $this->assertDatabaseMissing('items', $secondStoredItemData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testClearItemAndImageTable
     * @return void
     */
    public function testClearItemAndImageTable()
    {
        if (config('app.env') === 'testing') {
            Item::truncate();
            Image::truncate();
        }

        $this->assertDatabaseCount('items', 0);
        $this->assertDatabaseCount('images', 0);
    }
}
