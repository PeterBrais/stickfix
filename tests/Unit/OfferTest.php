<?php

namespace Tests\Unit;

use App\Enums\Language;
use App\Models\Offer;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

/**
 * Summary of OfferTest
 */
class OfferTest extends TestCase
{
    /**
     * Summary of testClearOfferTableAtBeginning
     * @return void
     */
    public function testClearOfferTableAtBeginning()
    {
        if (config('app.env') === 'testing') {
            Offer::truncate();
        }

        $this->assertDatabaseCount('offers', 0);
    }

    /**
     * Summary of testStoreOfferAsUnauthenticated
     * @return void
     */
    public function testStoreOfferAsUnauthenticated()
    {
        $offerData = [
            'title' => 'earing now saw perhaps minutes',
            'description' => 'nstantly excellent therefore difficult he northward',
            'language' => Language::EN,
            'isActive' => 1,
            'imageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/storeOffer', $offerData, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $storedOfferData = [
            'title' => $offerData['title'],
            'description' => $offerData['description'],
            'language' => $offerData['language'],
            'isActive' => $offerData['isActive'],
        ];

        $this->assertDatabaseMissing('offers', $storedOfferData);
    }

    /**
     * Summary of testStoreOfferAsAuthenticated
     * @return void
     */
    public function testStoreOfferAsAuthenticated()
    {
        $userData = [
            'username' => 'alda.dooley',
            'email' => 'alda.dooley@hotmail.com',
            'password' => 'alda.dooley123',
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $offerData = [
            'title' => 'Concerns greatest margaret',
            'description' => 'oor neat week do find past he. Be no surprise he honoured indulged',
            'language' => Language::EN,
            'isActive' => 1,
            'imageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $storedOfferData = [
            'title' => $offerData['title'],
            'description' => $offerData['description'],
            'language' => $offerData['language'],
            'isActive' => $offerData['isActive'],
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('POST', 'api/storeOffer', $offerData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
        $this->assertDatabaseHas('offers', $storedOfferData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        Offer::truncate();
        $this->assertDatabaseMissing('offers', $storedOfferData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testEditOfferAsUnauthenticated
     * @return void
     */
    public function testEditOfferAsUnauthenticated()
    {
        $offerData = [
            'title' => 'allowance instantly strangers',
            'description' => 'applauded discourse so. Separate entrance welcomed sensible laughing why one moderate shy',
            'language' => Language::EN,
            'isActive' => 1,
        ];

        $storedOfferData = [
            'title' => $offerData['title'],
            'description' => $offerData['description'],
            'language' => $offerData['language'],
            'isActive' => $offerData['isActive'],
        ];

        $offerObj = Offer::factory()->create($offerData);
        $this->assertDatabaseHas('offers', $storedOfferData);

        $editedOfferData = [
            'id' => $offerObj->id,
            'title' => 'We seeing piqued garden he',
            'description' => 'As in merry at forth least ye stood. And cold sons yet with',
            'language' => Language::LV,
            'isActive' => 1,
            'newImageCollection' => [],
            'storedImageCollection' => [],
            'deletedImageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $storedEditedOfferData = [
            'title' => $editedOfferData['title'],
            'description' => $editedOfferData['description'],
            'language' => $editedOfferData['language'],
            'isActive' => $editedOfferData['isActive'],
        ];

        $this->json('POST', 'api/editOffer', $editedOfferData, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $this->assertDatabaseHas('offers', $storedOfferData);
        $this->assertDatabaseMissing('offers', $storedEditedOfferData);

        $offerObj->delete();
        $this->assertDatabaseMissing('offers', $storedOfferData);
    }

    /**
     * Summary of testEditOfferAsAuthenticated
     * @return void
     */
    public function testEditOfferAsAuthenticated()
    {
        $userData = [
            'username' => 'viviane.abshire21',
            'email' => 'viviane.abshire21@hotmail.co',
            'password' => 'viviane.abshire21123',
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $offerData = [
            'title' => 'epending up believing',
            'description' => 'Enough around remove to barton agreed regret in or it',
            'language' => Language::EN,
            'isActive' => 1,
        ];

        $storedOfferData = [
            'title' => $offerData['title'],
            'description' => $offerData['description'],
            'language' => $offerData['language'],
            'isActive' => $offerData['isActive'],
        ];

        $offerObj = Offer::factory()->create($offerData);
        $this->assertDatabaseHas('offers', $storedOfferData);

        $editedOfferData = [
            'id' => $offerObj->id,
            'title' => 'shew come',
            'description' => 'Advantage mr estimable be commanded provision',
            'language' => Language::LV,
            'isActive' => 1,
            'newImageCollection' => [],
            'storedImageCollection' => [],
            'deletedImageCollection' => [],
            'recaptcha' => 'testing',
        ];

        $storedEditedOfferData = [
            'title' => $editedOfferData['title'],
            'description' => $editedOfferData['description'],
            'language' => $editedOfferData['language'],
            'isActive' => $editedOfferData['isActive'],
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('POST', 'api/editOffer', $editedOfferData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $this->assertDatabaseHas('offers', $storedEditedOfferData);
        $this->assertDatabaseMissing('offers', $storedOfferData);

        $offerObj->delete();
        $this->assertDatabaseMissing('offers', $storedEditedOfferData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testMarkOfferAsUnauthenticated
     * @return void
     */
    public function testMarkOfferAsUnauthenticated()
    {
        $offerData = [
            'title' => 'Written enquire',
            'description' => 'On ye position greatest so desirous',
            'language' => Language::EN,
            'isActive' => 1,
        ];

        $storedOfferData = [
            'title' => $offerData['title'],
            'description' => $offerData['description'],
            'language' => $offerData['language'],
            'isActive' => $offerData['isActive'],
        ];

        $offerObj = Offer::factory()->create($offerData);
        $this->assertDatabaseHas('offers', $storedOfferData);

        $this->json('GET', 'api/markOffer/' . $offerObj->id, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $offerObj->delete();
        $this->assertDatabaseMissing('offers', $storedOfferData);
    }

    /**
     * Summary of testMarkOfferAsAuthenticated
     * @return void
     */
    public function testMarkOfferAsAuthenticated()
    {
        $userData = [
            'username' => 'damon.weissnat',
            'email' => 'damon.weissnat@hotmail.com',
            'password' => Hash::make('damon.weissnat123'),
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $offerData = [
            'title' => 'comfort do written',
            'description' => 'elebrated contrasted discretion him sympathize her collecting occasional',
            'language' => Language::EN,
            'isActive' => true,
        ];

        $storedOfferData = [
            'title' => $offerData['title'],
            'description' => $offerData['description'],
            'language' => $offerData['language'],
            'isActive' => $offerData['isActive'],
        ];

        $offerObj = Offer::factory()->create($offerData);
        $this->assertDatabaseHas('offers', $storedOfferData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/markOffer/' . $offerObj->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('offers', $storedOfferData);

        $storedOfferData['isActive'] = false;
        $this->assertDatabaseHas('offers', $storedOfferData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $offerObj->delete();
        $this->assertDatabaseMissing('offers', $storedOfferData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testDeleteOfferAsUnauthenticated
     * @return void
     */
    public function testDeleteOfferAsUnauthenticated()
    {
        $userData = [
            'username' => 'reuben29',
            'email' => 'reuben29@gmail.com',
            'password' => Hash::make('reuben29123'),
            'isConfirmed' => 1,
        ];

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $offerData = [
            'title' => 'hough wished',
            'description' => 'waiting in on enjoyed placing it inquiry',
            'language' => Language::EN,
            'isActive' => 1,
        ];

        $storedOfferData = [
            'title' => $offerData['title'],
            'description' => $offerData['description'],
            'language' => $offerData['language'],
            'isActive' => $offerData['isActive'],
        ];

        $offerObj = Offer::factory()->create($offerData);
        $this->assertDatabaseHas('offers', $storedOfferData);

        $this->json('GET', 'api/deleteOffer/' . $offerObj->id, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.',
            ]);

        $offerObj->delete();
        $this->assertDatabaseMissing('offers', $storedOfferData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testDeleteOfferAsAuthenticated
     * @return void
     */
    public function testDeleteOfferAsAuthenticated()
    {
        $userData = [
            'username' => 'tate.koelpin',
            'email' => 'tate.koelpin@gmail.co',
            'password' => Hash::make('tate.koelpin123'),
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $offerData = [
            'title' => 'diminution',
            'description' => 'we diminution preference thoroughly',
            'language' => Language::EN,
            'isActive' => 1,
        ];

        $storedOfferData = [
            'title' => $offerData['title'],
            'description' => $offerData['description'],
            'language' => $offerData['language'],
            'isActive' => $offerData['isActive'],
        ];

        $offerObj = Offer::factory()->create($offerData);
        $this->assertDatabaseHas('offers', $storedOfferData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/deleteOffer/' . $offerObj->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->assertDatabaseMissing('offers', $storedOfferData);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testGetOfferById
     * @return void
     */
    public function testGetOfferById()
    {
        $offerData = [
            'title' => 'attending household',
            'description' => 'Middleton in objection discovery as agreeable',
            'language' => Language::EN,
            'isActive' => 1,
        ];

        $storedOfferData = [
            'title' => $offerData['title'],
            'description' => $offerData['description'],
            'language' => $offerData['language'],
            'isActive' => $offerData['isActive'],
        ];

        $offerObj = Offer::factory()->create($offerData);
        $this->assertDatabaseHas('offers', $storedOfferData);

        $this->json('GET', 'api/getOffer/' . $offerObj->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'offer' => [ 'id' => $offerObj->id ],
            ]);

        $offerObj->delete();
        $this->assertDatabaseMissing('offers', $storedOfferData);
    }

    /**
     * Summary of testGetOffers
     * @return void
     */
    public function testGetOffers()
    {
        Offer::truncate();

        $firstOfferData = [
            'title' => 'branched humanity',
            'description' => 'Ignorant branched humanity led now marianne too strongly entrance',
            'language' => Language::EN,
            'isActive' => 1,
        ];

        $firstStoredOfferData = [
            'title' => $firstOfferData['title'],
            'description' => $firstOfferData['description'],
            'language' => $firstOfferData['language'],
            'isActive' => $firstOfferData['isActive'],
        ];

        $firstOfferObj = Offer::factory()->create($firstOfferData);
        $this->assertDatabaseHas('offers', $firstStoredOfferData);

        $secondOfferData = [
            'title' => 'design',
            'description' => 'design are dinner better nearer silent excuse',
            'language' => null,
            'isActive' => 1,
        ];

        $secondStoredOfferData = [
            'title' => $secondOfferData['title'],
            'description' => $secondOfferData['description'],
            'language' => $secondOfferData['language'],
            'isActive' => $secondOfferData['isActive'],
        ];

        $secondOfferObj = Offer::factory()->create($secondOfferData);
        $this->assertDatabaseHas('offers', $secondStoredOfferData);

        $this->json('GET', 'api/getOffers', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'offers' => [
                    [ 'id' => $firstOfferObj->id ],
                    [ 'id' => $secondOfferObj->id ],
                ],
            ]);

        $firstOfferObj->delete();
        $this->assertDatabaseMissing('offers', $firstStoredOfferData);
        $secondOfferObj->delete();
        $this->assertDatabaseMissing('offers', $secondStoredOfferData);
    }

    /**
     * Summary of testClearServiceTableAtEnd
     * @return void
     */
    public function testClearServiceTableAtEnd()
    {
        if (config('app.env') === 'testing') {
            Offer::truncate();
        }

        $this->assertDatabaseCount('offers', 0);
    }
}
