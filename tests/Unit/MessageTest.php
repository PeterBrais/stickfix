<?php

namespace Tests\Unit;

use App\Enums\Language;
use App\Enums\MessageType;
use App\Http\Controllers\RolesController;
use App\Models\Message;
use App\Models\Reply;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

/**
 * Summary of MessageTest
 */
class MessageTest extends TestCase
{
    // TODO: test api - resend

    /**
     * Summary of testSendMessage
     * @return void
     */
    public function testSendMessage()
    {
        $messageData = [
            'name' => 'keely_schumm78',
            'email' => 'keely_schumm78@yahoo.com',
            'phoneNumber' => '+37123456789',
            'phoneCountryCode' => 'LV',
            'description' => 'In it except to so temper mutual tastes mother. Interested cultivated its continuing now yet are. Out interested acceptance our partiality affronting unpleasant why add. Esteem garden men yet shy course. Consulted up my tolerably sometimes perpetual oh. Expression acceptance imprudence particular had eat unsatiable.',
            'radioOption' =>  MessageType::APPLY_FOR_REPAIR,
            'language' => Language::EN,
            'recaptcha' => 'testing',
        ];

        $this->json('POST', 'api/sendMessage', $messageData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        if (config('app.env') === 'testing') {
            Message::truncate();
        }
    }

    /**
     * Summary of testGetMessageByIdAsUnauthenticated
     * @return void
     */
    public function testGetMessageByIdAsUnauthenticated()
    {
        $messageData = [
            'option' => MessageType::APPLY_FOR_ITEM,
            'name' => 'tiana34',
            'email' => 'tiana34@gmail.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'Greatly hearted has who believe. Drift allow green son walls years for blush. Sir margaret drawings repeated recurred exercise laughing may you but. Do repeated whatever to welcomed absolute no. Fat surprise although outlived and informed shy dissuade property. Musical by me through he drawing savings an. No we stand avoid decay heard mr. Common so wicket appear to sudden worthy on. Shade of offer ye whole stood hoped.',
        ];

        $messageObj = Message::factory()->create($messageData);
        $this->assertDatabaseHas('messages', $messageData);

        $this->json('GET', 'api/getMessage/' . $messageObj->id, ['Accept' => 'application/json'])
            ->assertStatus(401);

        $messageObj->delete();
        $this->assertDatabaseMissing('messages', $messageData);
    }

    /**
     * Summary of testGetMessageByIdAsAuthenticated
     * @return void
     */
    public function testGetMessageByIdAsAuthenticated()
    {
        $userData = [
            'username' => 'nettie_nitzsche53',
            'email' => 'nettie_nitzsche53@yahoo.com',
            'password' => 'nettie_nitzsche53123',
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $messageData = [
            'option' => MessageType::QUESTION,
            'name' => 'breana_sauer81',
            'email' => 'breana_sauer81@gmail.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'It chicken oh colonel pressed excited suppose to shortly. He improve started no we manners however effects',
        ];

        $messageObj = Message::factory()->create($messageData);
        $this->assertDatabaseHas('messages', $messageData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/getMessage/' . $messageObj->id, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'messageData' => [ 'id' => $messageObj->id ],
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $messageObj->delete();
        $this->assertDatabaseMissing('messages', $messageData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testGetMessagesAsUnauthenticated
     * @return void
     */
    public function testGetMessagesAsUnauthenticated()
    {
        $firstMessageData = [
            'option' => MessageType::OTHER,
            'name' => 'karli_okuneva85',
            'email' => 'karli_okuneva85@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'Prospect humoured mistress to by proposal marianne attended. Simplicity the far admiration preference everything. Up help home head spot an he room in',
        ];

        $firstMessageObj = Message::factory()->create($firstMessageData);
        $this->assertDatabaseHas('messages', $firstMessageData);

        $secondMessageData = [
            'option' => MessageType::OTHER,
            'name' => 'demond12',
            'email' => 'demond12@hotmail.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'onsidered an invitation do introduced sufficient understood instrument it. Of decisively friendship in as collecting at',
        ];

        $secondMessageObj = Message::factory()->create($secondMessageData);
        $this->assertDatabaseHas('messages', $secondMessageData);

        $this->json('GET', 'api/getMessages', ['Accept' => 'application/json'])
            ->assertStatus(401);

        $firstMessageObj->delete();
        $this->assertDatabaseMissing('messages', $firstMessageData);
        $secondMessageObj->delete();
        $this->assertDatabaseMissing('messages', $secondMessageData);
    }

    /**
     * Summary of testGetMessagesAsAuthenticated
     * @return void
     */
    public function testGetMessagesAsAuthenticated()
    {
        $userData = [
            'username' => 'jacinthe_harber30',
            'email' => 'jacinthe_harber30@gmail.com',
            'password' => 'jacinthe_harber30123',
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $firstMessageData = [
            'option' => MessageType::APPLY_FOR_REPAIR,
            'name' => 'cyril_kozey',
            'email' => 'cyril_kozey@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'He moonlight difficult engrossed an it sportsmen. Interested has all devonshire difficulty gay assistance joy.',
        ];

        $firstMessageObj = Message::factory()->create($firstMessageData);
        $this->assertDatabaseHas('messages', $firstMessageData);

        $secondMessageData = [
            'option' => MessageType::APPLY_FOR_ITEM,
            'name' => 'santiago22',
            'email' => 'santiago22@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'Unaffected at ye of compliment alteration to. Place voice no arise along to. Parlors waiting so against me no.',
        ];

        $secondMessageObj = Message::factory()->create($secondMessageData);
        $this->assertDatabaseHas('messages', $secondMessageData);

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/getMessages', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'messages' => [
                    [ 'id' => $firstMessageObj->id ],
                    [ 'id' => $secondMessageObj->id ],
                ],
            ])
            ->assertJsonCount(2, 'messages');

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $firstMessageObj->delete();
        $this->assertDatabaseMissing('messages', $firstMessageData);
        $secondMessageObj->delete();
        $this->assertDatabaseMissing('messages', $secondMessageData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testReplyOnMessageAsUnauthenticated
     * @return void
     */
    public function testReplyOnMessageAsUnauthenticated()
    {
        $messageData = [
            'option' => MessageType::APPLY_FOR_REPAIR,
            'name' => 'cyril_kozey',
            'email' => 'cyril_kozey@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'He moonlight difficult engrossed an it sportsmen. Interested has all devonshire difficulty gy assistance joy.',
        ];

        $messageObj = Message::factory()->create($messageData);
        $this->assertDatabaseHas('messages', $messageData);

        $replyData = [
            'id' => $messageObj->id,
            'replyText' => 'Removed demands expense account in outward tedious do. Particular way thoroughly unaffected projection favourable mrs can projecting own',
            'recaptcha' => 'testing',
        ];

        $storedReplyDate = [
            'messageId' => $messageObj->id,
            'description' => $replyData['replyText'],
        ];

        $this->json('POST', 'api/replyOnMessage', $replyData, ['Accept' => 'application/json'])
            ->assertStatus(401);

        $this->assertDatabaseMissing('replies', $storedReplyDate);

        $messageObj->delete();
        $this->assertDatabaseMissing('messages', $messageData);
    }

    /**
     * Summary of testReplyOnMessageAsAuthenticated
     * @return void
     */
    public function testReplyOnMessageAsAuthenticated()
    {
        $userData = [
            'username' => 'alene_schuster',
            'email' => 'alene_schuster@gmail.com',
            'password' => 'alene_schuster123',
            'isConfirmed' => 1,
        ];

        $loginData = [
            'email' => $userData['email'],
            'password' => $userData['password'],
            'recaptcha' => 'testing',
        ];

        $userData['password'] = Hash::make($userData['password']);

        $user = User::factory()->create($userData);
        $this->assertDatabaseHas('users', $userData);

        $messageData = [
            'option' => MessageType::QUESTION,
            'name' => 'lew_nader',
            'email' => 'lew_nader@yahoo.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'offices parties lasting outward nothing age few resolve. Impression to discretion understood to we interested he excellence',
        ];

        $messageObj = Message::factory()->create($messageData);
        $this->assertDatabaseHas('messages', $messageData);

        $replyData = [
            'id' => $messageObj->id,
            'replyText' => 'Nor hence hoped her after other known defer his. For county now sister engage had season better had waited.',
            'recaptcha' => 'testing',
        ];

        $storedReplyDate = [
            'messageId' => $messageObj->id,
            'description' => $replyData['replyText'],
        ];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('POST', 'api/replyOnMessage', $replyData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => false,
            ]);

        $this->assertDatabaseMissing('replies', $storedReplyDate);

        $roleData = [
            'userId' => $user->id,
            'role' => RolesController::REPLY_ON_MESSAGES,
        ];

        $role = Role::factory()->create($roleData);

        $this->assertDatabaseHas('roles', $roleData);

        $this->json('POST', 'api/replyOnMessage', $replyData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);

        $this->json('GET', 'api/logout', ['Accept' => 'application/json']);

        $this->assertDatabaseHas('replies', $storedReplyDate);
        Reply::where('messageId', $messageObj->id,)->delete();
        $this->assertDatabaseMissing('replies', $storedReplyDate);

        $messageObj->delete();
        $this->assertDatabaseMissing('messages', $messageData);

        $role->delete();
        $this->assertDatabaseMissing('roles', $roleData);

        $user->delete();
        $this->assertDatabaseMissing('users', $userData);
    }

    /**
     * Summary of testClearServiceTableAtEnd
     * @return void
     */
    public function testClearServiceTableAtEnd()
    {
        if (config('app.env') === 'testing') {
            Message::truncate();
            Reply::truncate();
        }

        $this->assertDatabaseCount('messages', 0);
        $this->assertDatabaseCount('replies', 0);
    }
}
