<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

/**
 * Summary of VerifyCsrfToken
 */
class VerifyCsrfToken extends Middleware
{
    /**
     * Summary of except
     * @var array<int, string>
     */
    protected $except = [
        '/api/webhook'
    ];
}
