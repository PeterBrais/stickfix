<?php

namespace App\Http\Controllers;

use App\Enums\CountryCode;
use App\Enums\DeliveryType;
use App\Enums\MailerType;
use App\Enums\OrderStatus;
use App\Mail\LowQuantity;
use App\Mail\NewOrder;
use App\Mail\NewOrderNotification;
use App\Mail\OrderSentOut;
use App\Models\Image;
use App\Models\Item;
use App\Models\Order;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception\InvalidOrderException;

/**
 * Summary of OrderController
 */
class OrderController extends Controller
{
    /**
     * Summary of makeOrder
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeOrder(Request $request)
    {
        $rules = [
            'products.selectedItemList' => 'required|array',
            'products.selectedItemList.*.id' => 'required|exists:items,id',
            'products.selectedItemList.*.title' => 'required|string|max:256',
            'products.selectedItemList.*.titleWithSizes' => 'required|string|max:1024',
            'products.selectedItemList.*.category' => 'required|string',
            'products.selectedItemList.*.size' => 'nullable',
            'products.selectedItemList.*.stickSize' => 'nullable',
            'products.selectedItemList.*.stickFlex' => 'nullable',
            'products.selectedItemList.*.bladeCurve' => 'nullable',
            'products.selectedItemList.*.bladeSide' => 'nullable',
            'products.selectedItemList.*.skateSize' => 'nullable',
            'products.selectedItemList.*.skateLength' => 'nullable',
            'products.selectedItemList.*.skateWidth' => 'nullable',
            'products.selectedItemList.*.priceInMinorUnit' => 'required|integer|min:1|max:2000000000',
            'products.selectedItemList.*.quantity' => 'required|integer|min:1',
            'products.selectedItemList.*.quantityWithPriceInMinorUnit' => 'required|integer|min:1|max:2000000000',
            'customer.firstName' => 'required|string|min:3|max:256',
            'customer.lastName' => 'required|string|min:3|max:256',
            'customer.email' => 'required|email|max:256',
            'customer.phoneNumber' => 'required|string|min:6|max:32',
            'customer.phoneCountryCode' => 'required',
            'customer.languageCode' => 'required',
            'shipping.deliveryType' => 'required|string',
            'shipping.country' => 'required|string|min:2|max:2',
            'shipping.byCourier.state' => 'nullable|max:256',
            'shipping.byCourier.city' => 'min:2|max:256',
            'shipping.byCourier.address' => 'min:2|max:256',
            'shipping.byCourier.zipCode' => 'max:32',
            'shipping.byParcelMachine.location.name' => 'max:256',
            'shipping.byParcelMachine.location.city' => 'max:256',
            'shipping.byParcelMachine.location.address' => 'max:256',
            'shipping.byParcelMachine.location.fullAddress' => 'max:256',
            'shipping.byParcelMachine.location.zipCode' => 'max:32',
            'shipping.pickUpInStore.address' => 'max:256',
            'amounts.itemAmountInMinorUnit' => 'required|integer|min:1|max:2000000000',
            'amounts.shippingAmountInMinorUnit' => 'required|integer|min:0|max:2000000000',
            'amounts.totalAmountInMinorUnit' => 'required|integer|min:1|max:2000000000',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'products.selectedItemList.required' => 'validation.requiredField',
            'products.selectedItemList.*.id' => [
                'required' => 'validation.requiredField',
                'array' => 'validation.requiredField',
                'exists' => 'validation.identifierDoesNotExist',
            ],
            'products.selectedItemList.*.title' => [
                'required' => 'validation.requiredField',
                'string' => 'validation.requiredField',
                'max' => 'validation.mustBe256CharsOrFewer',
            ],
            'products.selectedItemList.*.titleWithSizes' => [
                'required' => 'validation.requiredField',
                'string' => 'validation.requiredField',
                'max' => 'validation.mustBe1024CharsOrFewer',
            ],
            'products.selectedItemList.*.category' => [
                'required' => 'validation.requiredField',
                'string' => 'validation.requiredField',
            ],
            'products.selectedItemList.*.priceInMinorUnit' => [
                'required' => 'validation.requiredField',
                'integer' => 'validation.mayContainOnlyNaturalNumbers',
                'min' => 'validation.mustBeAPositiveValue',
                'max' => 'validation.numberTooLarge',
            ],
            'products.selectedItemList.*.quantity' => [
                'required' => 'validation.requiredField',
                'integer' => 'validation.mayContainOnlyNaturalNumbers',
                'min' => 'validation.mustBeAPositiveValue',
            ],
            'products.selectedItemList.*.quantityWithPriceInMinorUnit' => [
                'required' => 'validation.requiredField',
                'integer' => 'validation.mayContainOnlyNaturalNumbers',
                'min' => 'validation.mustBeAPositiveValue',
                'max' => 'validation.numberTooLarge',
            ],
            'customer.firstName.required' => 'validation.requiredField',
            'customer.firstName.string' => 'validation.requiredField',
            'customer.firstName.min' => 'validation.mustBeAtLeast3CharsLong',
            'customer.firstName.max' => 'validation.mustBe256CharsOrFewer',
            'customer.lastName.required' => 'validation.requiredField',
            'customer.lastName.string' => 'validation.requiredField',
            'customer.lastName.min' => 'validation.mustBeAtLeast3CharsLong',
            'customer.lastName.max' => 'validation.mustBe256CharsOrFewer',
            'customer.email.required' => 'validation.requiredField',
            'customer.email.email' => 'validation.invalidEmail',
            'customer.email.max' => 'validation.mustBe256CharsOrFewer',
            'customer.phoneNumber.required' => 'validation.requiredField',
            'customer.phoneNumber.string' => 'validation.requiredField',
            'customer.phoneNumber.min' => 'validation.invalidNumber',
            'customer.phoneNumber.max' => 'validation.invalidNumber',
            'customer.phoneCountryCode.required' => 'validation.invalidNumber',
            'customer.languageCode.required' => 'validation.requiredField',
            'shipping.deliveryType.required' => 'validation.requiredField',
            'shipping.deliveryType.string' => 'validation.requiredField',
            'shipping.country.required' => 'validation.requiredField',
            'shipping.country.string' => 'validation.requiredField',
            'shipping.country.min' => 'validation.mustBe2Chars',
            'shipping.country.max' => 'validation.mustBe2Chars',
            'shipping.byCourier.state.max' => 'validation.mustBe256CharsOrFewer',
            'shipping.byCourier.city.min' => 'validation.mustBeAtLeast2CharsLong',
            'shipping.byCourier.city.max' => 'validation.mustBe256CharsOrFewer',
            'shipping.byCourier.address.min' => 'validation.mustBeAtLeast2CharsLong',
            'shipping.byCourier.address.max' => 'validation.mustBe256CharsOrFewer',
            'shipping.byCourier.zipCode.max' => 'validation.mustBe32CharsOrFewer',
            'shipping.byParcelMachine.location.name.max' => 'validation.mustBe256CharsOrFewer',
            'shipping.byParcelMachine.location.city.max' => 'validation.mustBe256CharsOrFewer',
            'shipping.byParcelMachine.location.address.max' => 'validation.mustBe256CharsOrFewer',
            'shipping.byParcelMachine.location.fullAddress.max' => 'validation.mustBe256CharsOrFewer',
            'shipping.byParcelMachine.location.zipCode.max' => 'validation.mustBe32CharsOrFewer',
            'shipping.pickUpInStore.address.max' => 'validation.mustBe256CharsOrFewer',
            'shipping.pickUpInStore.zipCode.max' => 'validation.mustBe32CharsOrFewer',
            'amounts.itemAmountInMinorUnit.required' => 'validation.requiredField',
            'amounts.itemAmountInMinorUnit.integer' => 'validation.mayContainOnlyNaturalNumbers',
            'amounts.itemAmountInMinorUnit.min' => 'validation.mustBeAPositiveValue',
            'amounts.itemAmountInMinorUnit.max' => 'validation.numberTooLarge',
            'amounts.shippingAmountInMinorUnit.required' => 'validation.requiredField',
            'amounts.shippingAmountInMinorUnit.integer' => 'validation.mayContainOnlyNaturalNumbers',
            'amounts.shippingAmountInMinorUnit.min' => 'validation.mustBeAPositiveValue',
            'amounts.shippingAmountInMinorUnit.max' => 'validation.numberTooLarge',
            'amounts.totalAmountInMinorUnit.required' => 'validation.requiredField',
            'amounts.totalAmountInMinorUnit.integer' => 'validation.mayContainOnlyNaturalNumbers',
            'amounts.totalAmountInMinorUnit.min' => 'validation.mustBeAPositiveValue',
            'amounts.totalAmountInMinorUnit.max' => 'validation.numberTooLarge',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'error.pleaseTryAgainLater',
                'errors' => $validator->errors(),
            ]);
        }

        $uiProductList = $request->products['selectedItemList'];
        if (count($uiProductList) <= 0) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'error.pleaseTryAgainLater',
                'errors' => [ 'products' => [ 'selectedItemList' => ['validation.requiredField'] ] ],
            ]);
        }

        $itemAmountInMinorUnit = intval($request->amounts['itemAmountInMinorUnit']);
        if ($itemAmountInMinorUnit <= 0) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'error.pleaseTryAgainLater',
                'errors' => [ 'amounts' => [ 'itemAmountInMinorUnit' => [ 'validation.mayContainOnlyNaturalNumbers' ] ] ],
            ]);
        }

        $shippingAmountInMinorUnit = intval($request->amounts['shippingAmountInMinorUnit']);
        if ($shippingAmountInMinorUnit < 0) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'error.pleaseTryAgainLater',
                'errors' => [ 'amounts' => [ 'shippingAmountInMinorUnit' => [ 'validation.mayContainOnlyNaturalNumbers' ] ] ],
            ]);
        }

        $totalAmountInMinorUnit = intval($request->amounts['totalAmountInMinorUnit']);
        $areAmountsEqual = self::areAmountsEqual($itemAmountInMinorUnit, $shippingAmountInMinorUnit, $totalAmountInMinorUnit);
        if ($totalAmountInMinorUnit <= 0 || !$areAmountsEqual) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'error.pleaseTryAgainLater',
                'errors' => [ 'amounts' => [ 'totalAmountInMinorUnit' => [ 'validation.mayContainOnlyNaturalNumbers' ] ] ],
            ]);
        }

        $clientCountryCode = $request->shipping['country'];
        if (!HelperController::isValidCountryCode($clientCountryCode)) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'error.pleaseTryAgainLater',
                'errors' => [ 'shipping' => [ 'country' => ['validation.requiredField'] ] ],
            ]);
        }

        $deliveryType = $request->shipping['deliveryType'];
        if (!HelperController::isValidDeliveryType($deliveryType)) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'error.pleaseTryAgainLater',
                'errors' => [ 'shipping' => [ 'deliveryType' => ['validation.requiredField'] ] ],
            ]);
        }

        $parcelMachineName = '';
        $clientState = null;
        $clientCity = '';
        $clientFullAddress = '';
        $clientZipCode = '';
        if (HelperController::isParcelDeliveryType($deliveryType)) {
            $parcelMachineName = (string)$request->shipping['byParcelMachine']['location']['name'];
            $clientCity = (string)$request->shipping['byParcelMachine']['location']['city'];
            $clientAddress = (string)$request->shipping['byParcelMachine']['location']['address'];
            $clientFullAddress = (string)$request->shipping['byParcelMachine']['location']['fullAddress'];
            $clientZipCode = (string)$request->shipping['byParcelMachine']['location']['zipCode'];

            if (!strlen($parcelMachineName)
                || !strlen($clientCity)
                || !strlen($clientAddress)
                || !strlen($clientFullAddress)
            ) {
                return response()->json([
                    'success' => false,
                    'data' => null,
                    'message' => 'error.pleaseTryAgainLater',
                    'errors' => [ 'shipping' => [ 'byParcelMachine' => [ 'location' => ['validation.requiredField'] ] ] ],
                ]);
            }
        } else if (HelperController::isCourierDeliveryType($deliveryType)) {
            $clientState = (string)$request->shipping['byCourier']['state'];
            $clientCity = (string)$request->shipping['byCourier']['city'];
            $clientAddress = (string)$request->shipping['byCourier']['address'];
            $clientFullAddress = (string)self::getClientFullAddress($clientState, $clientCity, $clientAddress);
            $clientZipCode = (string)$request->shipping['byCourier']['zipCode'];

            if (!strlen($clientCity)) {
                return response()->json([
                    'success' => false,
                    'data' => null,
                    'message' => 'error.pleaseTryAgainLater',
                    'errors' => [ 'shipping' => [ 'byCourier' => [ 'city' => ['validation.requiredField'] ] ] ],
                ]);
            }

            if (!strlen($clientAddress) || !strlen($clientFullAddress)) {
                return response()->json([
                    'success' => false,
                    'data' => null,
                    'message' => 'error.pleaseTryAgainLater',
                    'errors' => [ 'shipping' => [ 'byCourier' => [ 'address' => ['validation.requiredField'] ] ] ],
                ]);
            }

            if (!strlen($clientZipCode)) {
                return response()->json([
                    'success' => false,
                    'data' => null,
                    'message' => 'error.pleaseTryAgainLater',
                    'errors' => [ 'shipping' => [ 'byCourier' => [ 'zipCode' => ['validation.requiredField'] ] ] ],
                ]);
            }
        } else {
            $clientAddress = (string)$request->shipping['pickUpInStore']['address'];
            $clientFullAddress = (string)$clientAddress;
            $clientZipCode = (string)$request->shipping['pickUpInStore']['zipCode'];

            if (!strlen($clientAddress)) {
                return response()->json([
                    'success' => false,
                    'data' => null,
                    'message' => 'error.pleaseTryAgainLater',
                    'errors' => [ 'shipping' => [ 'pickUpInStore' => [ 'address' => [ 'validation.requiredField' ] ] ] ],
                ]);
            }

            if (!strlen($clientZipCode)) {
                return response()->json([
                    'success' => false,
                    'data' => null,
                    'message' => 'error.pleaseTryAgainLater',
                    'errors' => [ 'shipping' => [ 'byCourier' => [ 'zipCode' => ['validation.requiredField'] ] ] ],
                ]);
            }
        }

        $clientZipCode = self::getModifiedZipCode($clientZipCode, $clientCountryCode);

        $preparedProductList = $this->prepareProductListFromUi($uiProductList);
        $encodedProductList = json_encode($preparedProductList);

        $stripe = new \Stripe\StripeClient(config('services.stripe.secretKey'));

        $lineItems = [];
        foreach ($preparedProductList as $product) {
            $productUrl = config('app.url') . substr($product['imageUrl'], 1);
            if (filter_var($productUrl, FILTER_VALIDATE_URL) === false) {
                $productUrl = config('app.url') . '/uploads/default.png';
            }

            $lineItems[] = [
                'price_data' => [
                    'currency' => 'eur',
                    'product_data' => [
                        'name' => $product['title'],
                        'images' => [ $productUrl ]
                    ],
                    'unit_amount' => $product['priceInMinorUnit']
                ],
                'quantity' => $product['quantity'],
            ];
        }

        $stripeSession = $stripe->checkout->sessions->create([
            'customer_email' => $request->customer['email'],
            'line_items' => $lineItems,
            'mode' => 'payment',
            'success_url' => config('app.url') . '/api/checkout/success?session_id={CHECKOUT_SESSION_ID}',
            'cancel_url' => config('app.url') . '/api/checkout/cancel?session_id={CHECKOUT_SESSION_ID}',
        ]);

        $order = new Order;
        $order->stripeSessionId = $stripeSession->id;
        $order->status = OrderStatus::INCOMPLETE;
        $order->productList = is_string($encodedProductList) ? $encodedProductList : '[]';
        $order->productAmountInMinorUnit = $itemAmountInMinorUnit;
        $order->shippingAmountInMinorUnit = $shippingAmountInMinorUnit;
        $order->totalAmountInMinorUnit = $totalAmountInMinorUnit;
        $order->clientFirstName = $request->customer['firstName'];
        $order->clientLastName = $request->customer['lastName'];
        $order->clientEmail = $request->customer['email'];
        $order->clientPhoneCountry = $request->customer['phoneCountryCode'];
        $order->clientPhoneNumber = $request->customer['phoneNumber'];
        $order->clientLanguageCode = $request->customer['languageCode'];
        $order->deliveryType = $deliveryType;
        $order->parcelMachineName = $parcelMachineName;
        $order->clientCountryCode = $clientCountryCode;
        $order->clientState = $clientState;
        $order->clientCity = $clientCity;
        $order->clientAddress = $clientAddress;
        $order->clientFullAddress = $clientFullAddress;
        $order->clientZipCode = $clientZipCode;
        $created = $order->save();

        if (!$created) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => 'error.pleaseTryAgainLater',
                'errors' => null
            ]);
        }

        return response()->json([
            'success' => true,
            'data' => [
                'url' => $stripeSession->url
            ],
            'message' => '',
            'errors' => null
        ]);
    }

    /**
     * Summary of success
     * @param \Illuminate\Http\Request $request
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function success(Request $request)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('services.stripe.secretKey'));
            $sessionId = (string)$_GET['session_id'];
            $stripe->checkout->sessions->retrieve($sessionId);

            $this->updateOrderAndSession($sessionId);
        } catch (InvalidOrderException $e) {
            return redirect(config('app.url') . '/checkout/success/' . $sessionId);
        } catch (\Exception $e) {
            return redirect(config('app.url') . '/404');
        }

        return redirect(config('app.url') . '/checkout/success/' . $sessionId);
    }

    /**
     * Summary of cancel
     * @param \Illuminate\Http\Request $request
     * @throws \Exception
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cancel(Request $request)
    {
        try {
            $stripe = new \Stripe\StripeClient(config('services.stripe.secretKey'));
            $sessionId = (string)$_GET['session_id'];
            $stripe->checkout->sessions->retrieve($sessionId);
        } catch (\Exception $e) {
            return redirect(config('app.url') . '/404');
        }

        try {
            $order = Order::where('stripeSessionId', $sessionId)->first();

            if (!$order) {
                throw new Exception('Order not found');
            }

            if ($order->status !== OrderStatus::INCOMPLETE) {
                throw new Exception('Wrong order status');
            }

            $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');
            $order->status = OrderStatus::FAILED;
            $order->processedAt = $dateTime->toDateTimeString();
            $updated = $order->save();

            if (!$updated) {
                throw new Exception('Failed to change order status');
            }
        } catch (\Exception $e) {
            return redirect(config('app.url') . '/404');
        }

        return redirect(config('app.url') . '/cart?type=error&message=checkout.cancelled');
    }

    /**
     * Summary of webhook
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function webhook(Request $request)
    {
        try {
            $payload = @file_get_contents('php://input');
            if (!$payload) {
                throw new \UnexpectedValueException('Invalid payload');
            }

            $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
            $endpointSecretKey = config('services.stripe.webhookSecretKey');

            $event = \Stripe\Webhook::constructEvent($payload, $sig_header, $endpointSecretKey);

            if ($event->type === 'checkout.session.completed') {
                $sessionObject = $event->data['object'];
                $sessionId = $sessionObject['id'];

                $this->updateOrderAndSession($sessionId);
            }
        } catch (InvalidOrderException $e) {
            return response()->json([ 'success' => true ]);
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            return response()->json([ 'success' => false ]);
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            return response()->json([ 'success' => false ]);
        } catch (\Exception $e) {
            return response()->json([ 'success' => false ]);
        }

        return response()->json([ 'success' => true ]);
    }

    /**
     * Summary of mark
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function mark(int $id)
    {
        try {
            /** @var Order $order */
            $order = Order::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        $order->isShownToClient = true;
        $updated = $order->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of getOrderBySession
     * @param string $sessionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSimpleOrderBySessionId($sessionId)
    {
        try {
            $order = Order::where('stripeSessionId', $sessionId)->first();
            $success = true;
            $message = null;

            if (!$order) {
                throw new Exception('Order not found');
            }

            if ($order->isShownToClient) {
                throw new Exception('Is already visited from FE');
            }

            $currentDateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');
            $orderDateTime = \Carbon\Carbon::parse($order->createdAt);
            $orderDateTimeOneDayLater = $orderDateTime->addDays(1);
            if ($currentDateTime->greaterThan($orderDateTimeOneDayLater)) {
                throw new Exception('Is already expired');
            }

            $orderData = [
                'id' => $order->id,
                'email' => $order->clientEmail,
                'productList' => json_decode($order->productList, true)
            ];
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.recordNotFound';
            $orderData = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'order' => $orderData,
        ]);
    }

    /**
     * Summary of getOrders
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrders()
    {
        try {
            if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::SEE_ORDERS)) {
                return response()->json([
                    'success' => false,
                    'message' => 'error.userHasNoPermission',
                ]);
            }

            $orders = Order::orderBy('createdAt', 'desc')->get();
            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $orders = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'orders' => $orders,
        ]);
    }

    /**
     * Summary of getOrder
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrder(int $id)
    {
        try {
            if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::SEE_ORDERS)) {
                return response()->json([
                    'success' => false,
                    'message' => 'error.userHasNoPermission',
                ]);
            }

            $order = Order::findOrFail($id);

            $orderData = [
                'id' => $order->id,
                'status' => $order->status,
                'productList' => json_decode($order->productList, true),
                'productAmountInMinorUnit' => $order->productAmountInMinorUnit,
                'shippingAmountInMinorUnit' => $order->shippingAmountInMinorUnit,
                'totalAmountInMinorUnit' => $order->totalAmountInMinorUnit,
                'clientFirstName' => $order->clientFirstName,
                'clientLastName' => $order->clientLastName,
                'clientEmail' => $order->clientEmail,
                'clientPhoneCountry' => $order->clientPhoneCountry,
                'clientPhoneNumber' => $order->clientPhoneNumber,
                'clientLanguageCode' => $order->clientLanguageCode,
                'deliveryType' => $order->deliveryType,
                'parcelMachineName' => $order->parcelMachineName,
                'clientCountryCode' => $order->clientCountryCode,
                'clientState' => $order->clientState,
                'clientCity' => $order->clientCity,
                'clientAddress' => $order->clientAddress,
                'clientFullAddress' => $order->clientFullAddress,
                'clientZipCode' => $order->clientZipCode,
                'createdAt' => $order->createdAt,
                'processedAt' => $order->processedAt,
                'sentOutAt' => $order->sentOutAt,
                'finishedAt' => $order->finishedAt,
                'lastOpenedAt' => $order->lastOpenedAt,
            ];

            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $orderData = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'order' => $orderData,
        ]);
    }

    /**
     * Summary of setOrderStatus
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setOrderStatus(Request $request)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::CHANGE_ORDER_STATUS)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        $rules = [
            'id' => 'required|exists:orders',
            'status' => 'required|string',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'status.required' => 'validation.requiredField',
            'status.string' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => $validator->errors(),
            ]);
        }

        if (!HelperController::isValidOrderStatus($request->status)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'status' => [ 'validation.requiredField' ] ],
            ]);
        }

        /** @var Order $order */
        $order = Order::find($request->id);

        if ($request->status === $order->status) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'status' => [ 'validation.requiredField' ] ],
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga')->toDateTimeString();

        if ($request->status === OrderStatus::PAID || $request->status === OrderStatus::FAILED) {
            $order->processedAt = $dateTime;
        } else if ($request->status === OrderStatus::SENT_OUT) {
            $this->sendSentOutNotification($order);
            $order->sentOutAt = $dateTime;
        } else if ($request->status === OrderStatus::FINISHED) {
            $order->finishedAt = $dateTime;
        }

        $order->status = $request->status;
        $updated = $order->save();

        if (!$updated) {
            return response()->json([
                'success' => false,
                'message' => 'error.databaseError',
                'errors' => null
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'statusUpdated',
            'errors' => null
        ]);
    }

    /**
     * Summary of updateLastOpenedAt
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLastOpenedAt(Request $request)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::SEE_ORDERS)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        $rules = [
            'id' => 'required|exists:orders',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => $validator->errors(),
            ]);
        }

        /** @var Order $order */
        $order = Order::find($request->id);

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga')->toDateTimeString();
        $order->lastOpenedAt = $dateTime;
        $updated = $order->save();

        if (!$updated) {
            return response()->json([
                'success' => false,
                'message' => 'error.databaseError',
                'errors' => null
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'dataUpdated',
            'errors' => null
        ]);
    }

    /**
     * Summary of areAmountsEqual
     * @param int $productAmountInMinorUnit
     * @param int $shippingAmountInMinorUnit
     * @param int $totalAmountInMinorUnit
     * @return bool
     */
    private static function areAmountsEqual(int $productAmountInMinorUnit, int $shippingAmountInMinorUnit, int $totalAmountInMinorUnit) {
        return ($productAmountInMinorUnit + $shippingAmountInMinorUnit) === $totalAmountInMinorUnit;
    }

    /**
     * Summary of getClientFullAddress
     * @param string $state
     * @param string $city
     * @param string $address
     * @return string
     */
    private static function getClientFullAddress(string $state, string $city, string $address): string
    {
        $clientFullAddress = '';

        if (strlen($state) > 0) {
            $clientFullAddress .= $state . ', ';
        }

        $clientFullAddress .= $city . ', ' . $address;

        return trim($clientFullAddress);
    }

    /**
     * Summary of prepareProductListFromUi
     * @param array<int, mixed> $uiProductList
     * @return array<int, mixed>
     */
    private function prepareProductListFromUi(array $uiProductList): array
    {
        $preparedProductList = [];

        foreach ($uiProductList as $uiProduct) {
            if (!$uiProduct['isDataRow']) {
                continue;
            }

            $preparedProductList[] = array(
                'productId' => $uiProduct['id'],
                'title' => $uiProduct['title'],
                'titleWithSizes' => $uiProduct['titleWithSizes'],
                'category' => $uiProduct['category'],
                'size' => $uiProduct['size'],
                'stickSize' => $uiProduct['stickSize'],
                'stickFlex' => $uiProduct['stickFlex'],
                'bladeCurve' => $uiProduct['bladeCurve'],
                'bladeSide' => $uiProduct['bladeSide'],
                'skateSize' => $uiProduct['skateSize'],
                'skateLength' => $uiProduct['skateLength'],
                'skateWidth' => $uiProduct['skateWidth'],
                'imageUrl' => $uiProduct['imageUrl'],
                'priceInMinorUnit' => $uiProduct['priceInMinorUnit'],
                'quantity' => $uiProduct['quantity'],
                'quantityWithPriceInMinorUnit' => $uiProduct['quantityWithPriceInMinorUnit'],
            );
        }

        return $preparedProductList;
    }

    /**
     * Summary of prepareProductListForMail
     * @param array<int, mixed> $productList
     * @return array<int, mixed>
     */
    private function prepareProductListForMail(array $productList): array
    {
        $preparedProductList = [];

        foreach ($productList as $product) {
            $preparedProductList[] = array(
                'productId' => $product['productId'],
                'title' => $product['title'],
                'titleWithSizes' => $product['titleWithSizes'],
                'imageUrl' => $product['imageUrl'],
                'price' => number_format((int)$product['priceInMinorUnit'] / 100, 2),
                'quantity' => $product['quantity'],
                'quantityWithPrice' => number_format((int)$product['quantityWithPriceInMinorUnit'] / 100, 2),
            );
        }

        return $preparedProductList;
    }

    /**
     * Summary of decreaseProductLimit
     * @param array<int, mixed> $productList
     * @return void
     */
    private function decreaseProductLimit(array $productList): void
    {
        foreach ($productList as $product) {
            try {
                /** @var Item $item */
                $item = Item::find($product['productId']);

                $isActive = true;
                $newQuantity = $item->quantity - intval($product['quantity']);
                if ($newQuantity <= 0) {
                    $newQuantity = 0;
                    $isActive = false;

                    $image = Image::where(ImageController::MODEL_ITEM_ID, $item->id)->first();

                    $imageUrl = null;
                    $isImageAvailable = false;
                    if ($image) {
                        $imageUrl = $image['path'] . $image['name'];
                        $isImageAvailable = true;
                    }

                    $mailData = [
                        'itemId' => $item->id,
                        'itemTitle' => $item->title,
                        'itemPrice' => number_format($item->priceInMinorUnit / 100, 2),
                        'imageUrl' => $imageUrl,
                        'isImageAvailable' => $isImageAvailable,
                        'requisites' => MessageController::getRequisites(),
                    ];

                    MessageController::sendEmail(new LowQuantity($mailData), MailerType::NOREPLY);
                }

                $item->quantity = $newQuantity;
                $item->isActive = $isActive;

                $item->save();
            } catch (\Exception $e) {

            }
        }
    }

    /**
     * Summary of sendPurchaseNotification
     * @param \App\Models\Order $order
     * @throws \Exception
     * @return void
     */
    private function sendPurchaseNotification(Order $order): void
    {
        if ($order->isNotificationSent) {
            return;
        }

        try {
            $processedAtDate = \Carbon\Carbon::parse($order->processedAt)->toDateString();

            $newOrderMailData = [
                'orderId' => $order->id,
                'orderDate' => $processedAtDate,
                'orderStatus' => 'Jauns pasūtījums',
                'clientName' => $order->clientFirstName . ' ' . $order->clientLastName,
                'clientEmail' => $order->clientEmail,
                'clientPhoneNumber' => $order->clientPhoneNumber,
                'deliveryMethod' => $this->getTranslatedDeliveryType($order->deliveryType),
                'deliveryAddress' => $this->getFullDeliveryAddress($order),
                'productList' => $this->prepareProductListForMail(json_decode($order->productList, true)),
                'productAmount' => number_format($order->productAmountInMinorUnit / 100, 2),
                'shippingAmount' => number_format($order->shippingAmountInMinorUnit / 100, 2),
                'totalAmount' => number_format($order->totalAmountInMinorUnit / 100, 2),
                'requisites' => MessageController::getRequisites(),
            ];

            MessageController::sendEmail(new NewOrder($newOrderMailData), MailerType::NOREPLY);
            MessageController::sendEmail(new NewOrderNotification($newOrderMailData), MailerType::NOREPLY);

            $order->isNotificationSent = true;
            $updated = $order->save();

            if (!$updated) {
                throw new Exception('Failed to change order status');
            }
        } catch (\Exception $e) {

        }
    }

    /**
     * Summary of sendSentOutNotification
     * @param \App\Models\Order $order
     * @return void
     */
    private function sendSentOutNotification(Order $order): void
    {
        if (!is_null($order->sentOutAt)) {
            return;
        }

        try {
            $orderSentOutMailData = [
                'orderId' => $order->id,
                'clientName' => $order->clientFirstName . ' ' . $order->clientLastName,
                'clientEmail' => $order->clientEmail,
                'requisites' => MessageController::getRequisites(),
            ];

            MessageController::sendEmail(new OrderSentOut($orderSentOutMailData), MailerType::NOREPLY);
        } catch (\Exception $e) {

        }
    }

    /**
     * Summary of getTranslatedDeliveryType
     * @param string $deliveryType
     * @return string
     */
    private function getTranslatedDeliveryType(string $deliveryType): string
    {
        if ($deliveryType === DeliveryType::OMNIVA) {
            return 'OMNIVA';
        } else if ($deliveryType === DeliveryType::COURIER) {
            return 'Kurjers';
        } else if ($deliveryType === DeliveryType::PICK_UP_IN_STORE_FOR_FREE) {
            return 'Saņemšana mūsu veikalā';
        }

        return $deliveryType;
    }

    /**
     * Summary of getFullDeliveryAddress
     * @param \App\Models\Order $order
     * @return string
     */
    private function getFullDeliveryAddress(Order $order): string
    {
        if ($order->deliveryType === DeliveryType::OMNIVA) {
            $parcelMachineName = '';
            if (strlen(trim($order->parcelMachineName)) > 0) {
                $parcelMachineName = ' (' . $order->parcelMachineName . ')';
            }
            return $order->clientFullAddress . ', ' . $order->clientZipCode . $parcelMachineName;
        } else if ($order->deliveryType === DeliveryType::COURIER
            || $order->deliveryType === DeliveryType::PICK_UP_IN_STORE_FOR_FREE
        ) {
            return $order->clientFullAddress . ', ' . $order->clientZipCode;
        }

        return $order->clientFullAddress;
    }

    /**
     * Summary of getModifiedZipCode
     * @param string $clientZipCode
     * @param string $clientCountryCode
     * @return string
     */
    private static function getModifiedZipCode(string $clientZipCode, string $clientCountryCode): string
    {
        if (!preg_match("/^\d+$/", $clientZipCode)) {
            return $clientZipCode;
        }

        $modifiedClientCountryCode = strtoupper($clientCountryCode);
        if ($modifiedClientCountryCode === CountryCode::LV || $modifiedClientCountryCode === CountryCode::LT) {
            return $modifiedClientCountryCode . '-' . $clientZipCode;
        }

        return $clientZipCode;
    }

    /**
     * Summary of updateOrderAndSession
     * @param string $sessionId
     * @throws \Exception
     * @return void
     */
    private function updateOrderAndSession(string $sessionId): void
    {
        $order = Order::where('stripeSessionId', $sessionId)->first();

        if (!$order) {
            throw new Exception('Order not found');
        }

        if ($order->status === OrderStatus::PAID) {
            throw new InvalidOrderException('Order not found');
        }

        if ($order->status !== OrderStatus::INCOMPLETE) {
            throw new Exception('Wrong order status');
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');
        $order->status = OrderStatus::PAID;
        $order->processedAt = $dateTime->toDateTimeString();
        $updated = $order->save();

        if (!$updated) {
            throw new Exception('Failed to change order status');
        }

        $orderedProductList = json_decode($order->productList, true);
        $this->decreaseProductLimit($orderedProductList);

        self::sendPurchaseNotification($order);
    }
}
