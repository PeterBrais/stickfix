<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ReCaptcha3;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\UserController;
use App\Models\User;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Summary of RegisterController
 */
class RegisterController extends Controller
{
    /**
     * Summary of store
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::ADD_NEW_USER)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        $rules = [
            'username' => 'required|min:3|max:64|unique:users',
            'email' => 'required|email|max:256|unique:users',
            'password' => 'required|min:8|max:1024|confirmed',
            'language' => 'required|min:2|max:2',
            'isBlocked' => 'boolean',
            'roles' => 'array',
            'roles.*' => 'required',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'username.required' => 'validation.requiredField',
            'username.min' => 'validation.mustBeAtLeast3CharsLong',
            'username.max' => 'validation.mustBe64CharsOrFewer',
            'username.unique' => 'validation.usernameAlreadyExists',
            'email.required' => 'validation.requiredField',
            'email.email' => 'validation.invalidEmail',
            'email.max' => 'validation.mustBe256CharsOrFewer',
            'email.unique' => 'validation.emailAlreadyExists',
            'password.required' => 'validation.requiredField',
            'password.min' => 'validation.mustBeAtLeast8CharsLong',
            'password.max' => 'validation.mustBe1024CharsOrFewer',
            'password.confirmed' => 'validation.passwordsDoNotMatch',
            'language.required' => 'validation.requiredField',
            'language.min' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'isBlocked.boolean' => 'validation.requiredField',
            'roles.required' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors()
            ]);
        }

        if (!UserController::isValidLanguage($request->language)) {
            return response()->json([
                'success' => false,
                'message' => 'error.validationError',
                'errors' => [ 'language' => ['validation.requiredField'] ],
            ]);
        }

        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->language = $request->language;
        $user->isBlocked = $request->isBlocked;
        $created = $user->save();

        if (!$created) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        $roleErrors = RolesController::storeNewUserRoles($request->roles, $user->id);

        if (config('app.env') === 'production') {
            event(new Registered($user));
        }

        return response()->json([
            'success' => true, 'message' => 'recordAdded', 'errors' => $roleErrors
        ]);
    }

    /**
     * Summary of checkEmailAvailability
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkEmailAvailability(Request $request)
    {
        $rules = [
            'email' => 'unique:users',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'available' => false
            ]);
        }

        return response()->json([
            'available' => true
        ]);
    }

    /**
     * Summary of checkUsernameAvailability
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkUsernameAvailability(Request $request)
    {
        $rules = [
            'username' => 'unique:users',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'available' => false
            ]);
        }

        return response()->json([
            'available' => true
        ]);
    }

    /**
     * Summary of verifyViaHash
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verifyViaHash(Request $request): RedirectResponse
    {
        /** @var User $user */
        $user = User::find($request->route('id'));

        if ($user->hasVerifiedEmail()) {
            return redirect(config('app.url') . '/verified/' . $user->id);
        }

        if ($user->markEmailAsVerified()) {
            $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');
            $user->isConfirmed = true;
            $user->editedAt = $dateTime->toDateTimeString();
            $user->save();

            if (config('app.env') === 'production') {
                event(new Verified($user));
            }
        }

        return redirect(config('app.url') . '/verified/' . $user->id);
    }

    /**
     * Summary of confirmViaId
     * @param int $id
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmViaId(int $id)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::CONFIRM_USER)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        try {
            if (UserController::isDefaultUser($id)) {
                throw new Exception();
            }
            /** @var User $user */
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false, 'message' => 'error.error', 'errors' => null
            ]);
        }

        if ($user->isConfirmed) {
            return response()->json([
                'success' => false, 'message' => 'error.userAlreadyConfirmed', 'errors' => null
            ]);
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        $user->isConfirmed = true;
        $user->editedAt = $dateTime->toDateTimeString();
        $updated = $user->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of resendVerification
     * @param int $id
     * @throws \Exception
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendVerification(int $id)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::CONFIRM_USER)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        try {
            if (UserController::isDefaultUser($id)) {
                throw new Exception();
            }
            /** @var User $user */
            $user = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false, 'message' => 'error.error', 'errors' => null
            ]);
        }

        if ($user->isConfirmed) {
            return response()->json([
                'success' => false, 'message' => 'error.userAlreadyConfirmed', 'errors' => null
            ]);
        }

        $user->isShownToUser = false;
        $user->save();

        $user->sendEmailVerificationNotification();

        return response()->json([
            'success' => true, 'message' => 'emailSent', 'errors' => null
        ]);
    }

    /**
     * Summary of setVerificationAsShown
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function setVerificationAsShown(int $id)
    {
        try {
            /** @var User $user */
            $user = User::findOrFail($id);

            if (!$user->isShownToUser) {
                $user->isShownToUser = true;
                $updated = $user->save();

                if (!$updated) {
                    return response()->json([
                        'success' => false
                    ]);
                }
            }

            return response()->json([
                'success' => true
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false
            ]);
        }
    }
}
