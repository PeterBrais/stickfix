<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ImageController;
use App\Http\Controllers\ReCaptcha3;
use App\Models\Offer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

/**
 * Summary of OfferController
 */
class OfferController extends Controller
{
    /**
     * Summary of getOffers
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOffers()
    {
        try {
            if (Auth::check()) {
                $offers = Offer::with('images')->orderBy('createdAt', 'desc')->get();
            } else {
                if (Cache::has('getOffersGuest')) {
                    $offers = Cache::get('getOffersGuest');
                } else {
                    $offers = Offer::with('images')->where('isActive', true)->orderBy('createdAt', 'desc')->get();
                    Cache::put('getOffersGuest', $offers, now()->addMinutes(5));
                }
            }

            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $offers = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'offers' => $offers,
        ]);
    }

    /**
     * Summary of getOffer
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOffer(int $id)
    {
        try {
            $offer = Offer::with('images')->findOrFail($id);
            $success = true;
            $message = null;
        } catch (ModelNotFoundException $e) {
            $success = false;
            $message = 'error.recordNotFound';
            $offer = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'offer' => $offer,
        ]);
    }

    /**
     * Summary of store
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|min:3|max:256',
            'description' => 'max:4096',
            'language' => 'string|max:2|nullable',
            'isActive' => 'boolean',
            'imageCollection' => 'array',
            'imageCollection.*.base64' => 'required',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'language.string' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'isActive.boolean' => 'validation.requiredField',
            'imageCollection.required' => 'validation.requiredField',
            'imageCollection.*.base64' => [
                'required' => 'validation.requiredField',
            ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        $preparedLanguageValue = null;
        if (HelperController::isValidLanguageType($request->language)) {
            $preparedLanguageValue = $request->language;
        }

        $offer = new Offer();
        $offer->title = $request->title;
        $offer->description = $request->description;
        $offer->language = $preparedLanguageValue;
        $offer->isActive = $request->isActive;
        $created = $offer->save();

        if (!$created) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        $path = ImageController::getObjectPath(ImageController::OFFER_FOLDER_NAME, $offer->id);
        $imageErrors = ImageController::store($request->imageCollection, $path, $offer);

        return response()->json([
            'success' => true, 'message' => 'recordAdded', 'errors' => $imageErrors
        ]);
    }

    /**
     * Summary of mark
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function mark(int $id)
    {
        try {
            /** @var Offer $offer */
            $offer = Offer::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        $offer->isActive = !$offer->isActive;
        $updated = $offer->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of delete
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        try {
            /** @var Offer $offer */
            $offer = Offer::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        $deleted = $offer->delete();

        if (!$deleted) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        ImageController::deleteAllModelImages(ImageController::MODEL_OFFER, $id);

        return response()->json([
            'success' => true, 'message' => 'recordRemoved', 'errors' => null
        ]);
    }

    /**
     * Summary of edit
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request)
    {
        $rules = [
            'id' => 'required|exists:offers',
            'title' => 'required|min:3|max:256',
            'description' => 'max:4096',
            'language' => 'string|max:2|nullable',
            'isActive' => 'boolean',
            'newImageCollection' => 'array',
            'newImageCollection.*.base64' => 'required',
            'storedImageCollection' => 'array',
            'storedImageCollection.*.id' => 'required|exists:images',
            'storedImageCollection.*.path' => 'required',
            'storedImageCollection.*.name' => 'required',
            'deletedImageCollection' => 'array',
            'deletedImageCollection.*.id' => 'required|exists:images',
            'deletedImageCollection.*.path' => 'required',
            'deletedImageCollection.*.name' => 'required',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'id.required' => 'validation.identifierDoesNotExist',
            'id.exists' => 'validation.identifierDoesNotExist',
            'title.required' => 'validation.requiredField',
            'title.min' => 'validation.mustBeAtLeast3CharsLong',
            'title.max' => 'validation.mustBe256CharsOrFewer',
            'description.max' => 'validation.mustBe4096CharsOrFewer',
            'language.string' => 'validation.mustBe2Chars',
            'language.max' => 'validation.mustBe2Chars',
            'isActive.boolean' => 'validation.requiredField',
            'newImageCollection.required' => 'validation.requiredField',
            'newImageCollection.*.base64' => [ 'required' => 'validation.requiredField' ],
            'storedImageCollection.required' => 'validation.storedImagesDoesNotExist',
            'storedImageCollection.*.id' => [
                'required' => 'validation.requiredField',
                'exists' => 'validation.storedImagesIdentifierDoesNotExist'
            ],
            'storedImageCollection.*.path' => [ 'required' => 'validation.requiredField' ],
            'storedImageCollection.*.name' => [ 'required' => 'validation.requiredField' ],
            'deletedImageCollection.required' => 'validation.deletedImagesDoesNotExist',
            'deletedImageCollection.*.id' => [
                'required' => 'validation.requiredField',
                'exists' => 'validation.deletedImagesIdentifierDoesNotExist'
            ],
            'deletedImageCollection.*.path' => [ 'required' => 'validation.requiredField' ],
            'deletedImageCollection.*.name' => [ 'required' => 'validation.requiredField' ],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        $preparedLanguageValue = null;
        if (HelperController::isValidLanguageType($request->language)) {
            $preparedLanguageValue = $request->language;
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');

        /** @var Offer $offer */
        $offer = Offer::find($request->id);
        $offer->title = $request->title;
        $offer->description = $request->description;
        $offer->language = $preparedLanguageValue;
        $offer->isActive = $request->isActive;
        $offer->editedAt = $dateTime->toDateTimeString();
        $updated = $offer->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        $path = ImageController::getObjectPath(ImageController::OFFER_FOLDER_NAME, $offer->id);
        ImageController::deleteSelectedImages($request->deletedImageCollection, $path);
        $imageErrors = ImageController::store($request->newImageCollection, $path, $offer);

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => $imageErrors
        ]);
    }
}
