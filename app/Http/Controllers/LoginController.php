<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ReCaptcha3;
use App\Http\Controllers\RolesController;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

/**
 * Summary of LoginController
 */
class LoginController extends Controller
{
    /**
     * Summary of login
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $message = 'error.incorrectCredentials';
        $rules = [
            'email' => 'required',
            'password' => 'required',
            'recaptcha' => 'required|string',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => $message
            ]);
        }

        $loginType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if (Auth::attempt([$loginType => $request->email, 'password' => $request->password, 'isConfirmed' => 1, 'isBlocked' => 0])) {
            Auth::user();
            return response()->json([
                'success' => true,
            ]);
        }

        return response()->json([
            'success' => false, 'message' => $message
        ]);
    }

    /**
     * Summary of logout
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        if (Auth::check()) {
            Session::flush();
            Auth::logout();
        }

        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * Summary of checkAuth
     * @param mixed $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkAuth($role = null)
    {
        if (Auth::check()) {
            /** @var User $user */
            $user = Auth::user();
            if ($user->isBlocked || !$user->isConfirmed) {
                $this->logout();
                return response()->json([
                    'success' => false,
                ]);
            }

            if ($role !== null) {
                $userRoles = Role::where('userId', $user->id)->get();
                return response()->json([
                    'success' => RolesController::checkIfUserHasRole($userRoles, $role),
                ]);
            } else {
                return response()->json([
                    'success' => true,
                ]);
            }
        }

        return response()->json([
            'success' => false,
        ]);
    }
}
