<?php

namespace App\Http\Controllers;
use App\Enums\BagSize;
use App\Enums\BrandType;
use App\Enums\CategoryType;
use App\Enums\ConditionType;
use App\Enums\CountryCode;
use App\Enums\DeliveryType;
use App\Enums\GenderType;
use App\Enums\HelmetSize;
use App\Enums\KitSize;
use App\Enums\Language;
use App\Enums\MessageType;
use App\Enums\OrderStatus;
use App\Enums\PadsAndPantsSize;
use App\Enums\ServiceType;
use App\Enums\SkateLengthSize;
use App\Enums\SkateSize;
use App\Enums\SkateWidthSize;
use App\Enums\StickBladeCurveType;
use App\Enums\StickBladeSideType;
use App\Enums\StickSize;

/**
 * Summary of Enums
 */
class Enums extends Controller
{
    /**
     * Summary of getCategories
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories()
    {
        return response()->json(CategoryType::getValues());
    }

    /**
     * Summary of getConditions
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConditions()
    {
        return response()->json(ConditionType::getValues());
    }

    /**
     * Summary of getMessageOptions
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMessageOptions()
    {
        return response()->json(MessageType::getValues());
    }

    /**
     * Summary of getServiceType
     * @return \Illuminate\Http\JsonResponse
     */
    public function getServiceType()
    {
        return response()->json(ServiceType::getValues());
    }

    /**
     * Summary of getBrands
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBrands()
    {
        return response()->json(BrandType::getValues());
    }

    /**
     * Summary of getLanguages
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLanguages()
    {
        return response()->json(Language::getValues());
    }

    /**
     * Summary of getGenders
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGenders()
    {
        return response()->json(GenderType::getValues());
    }

    /**
     * Summary of getCountryCodes
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountryCodes()
    {
        return response()->json(CountryCode::getValues());
    }

    /**
     * Summary of getOrderStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderStatus()
    {
        return response()->json(OrderStatus::getValues());
    }

    /**
     * Summary of getDeliveryTypes
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeliveryTypes()
    {
        return response()->json(DeliveryType::getValues());
    }

    /**
     * Summary of getSizes
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSizes()
    {
        return response()->json([
            'SHIN_GUARD' => $this->getInterval(8, 17), // Shin guards: 8 - 17
            'PANTS_SHOULDER_ELBOW_PAD_SIZES' => PadsAndPantsSize::getValues(),
            'SKATES' => array(
                'SIZE' => SkateSize::getValues(),
                'WIDTH' => SkateWidthSize::getValues(),
                'LENGTH' => $this->getSkateLengthSizes(),
            ),
            'STICK' => array(
                'SIZE' => StickSize::getValues(),
                'BLADE_CURVE' => StickBladeCurveType::getValues(),
                'BLADE_SIDE' => StickBladeSideType::getValues(),
                'FLEX' => $this->getInterval(1, 150), // Flex: 1 - 150
            ),
            'HELMET' => HelmetSize::getValues(),
            'GLOVES' => $this->getInterval(8, 15), // Gloves: 8 - 15
            'BAG' => BagSize::getValues(),
            'KIT' => KitSize::getValues(),
        ]);
    }

    /**
     * Summary of getAllEnums
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllEnums()
    {
        return response()->json([
            'CATEGORIES' => CategoryType::getValues(),
            'CONDITIONS' => ConditionType::getValues(),
            'MESSAGE_OPTIONS' => MessageType::getValues(),
            'SERVICE_TYPES' => ServiceType::getValues(),
            'BRANDS' => BrandType::getValues(),
            'SIZES' => $this->getSizes(),
            'LANGUAGES' => Language::getValues(),
            'GENDERS' => GenderType::getValues(),
            'COUNTRY_CODES' => CountryCode::getValues(),
            'DELIVERY_TYPES' => DeliveryType::getValues(),
        ]);
    }

    /**
     * Summary of getInterval
     * @param int $min
     * @param int $max
     * @return array<int>
     */
    public function getInterval(int $min, int $max): array
    {
        $flex = [];
        for ($i = $min; $i <= $max; $i++) {
            $flex[] = $i;
        }
        return $flex;
    }

    /**
     * Summary of getSkateLengthSizes
     * @return array<string, mixed>
     */
    public function getSkateLengthSizes(): array
    {
        return array(
            SkateSize::SR => $this->getSrSkateLengthSizes(),
            SkateSize::INT => $this->getIntSkateLengthSizes(),
            SkateSize::JR => $this->getJrSkateLengthSizes(),
            SkateSize::YTH => $this->getYthSkateLengthSizes()
        );
    }

    /**
     * Summary of getSrSkateLengthSizes
     * @return array<string, mixed>
     */
    public function getSrSkateLengthSizes(): array
    {
        return array(
            SkateLengthSize::SR_7_0 => [
                'US' => '8.5',
                'EUR' => '42.0',
                'UK' => '7.5',
            ],
            SkateLengthSize::SR_7_5 => [
                'US' => '9.0',
                'EUR' => '42.5',
                'UK' => '8.0',
            ],
            SkateLengthSize::SR_8_0 => [
                'US' => '9.5',
                'EUR' => '43.0',
                'UK' => '8.5',
            ],
            SkateLengthSize::SR_8_5 => [
                'US' => '10.0',
                'EUR' => '44.0',
                'UK' => '9.0',
            ],
            SkateLengthSize::SR_9_0 => [
                'US' => '10.5',
                'EUR' => '44.5',
                'UK' => '9.5',
            ],
            SkateLengthSize::SR_9_5 => [
                'US' => '11.0',
                'EUR' => '45.0',
                'UK' => '10.0',
            ],
            SkateLengthSize::SR_10_0 => [
                'US' => '11.5',
                'EUR' => '45.5',
                'UK' => '10.5',
            ],
            SkateLengthSize::SR_10_5 => [
                'US' => '12.0',
                'EUR' => '46.0',
                'UK' => '11.0',
            ],
            SkateLengthSize::SR_11_0 => [
                'US' => '12.5',
                'EUR' => '47.0',
                'UK' => '11.5',
            ],
            SkateLengthSize::SR_11_5 => [
                'US' => '13.0',
                'EUR' => '47.5',
                'UK' => '12.0',
            ],
            SkateLengthSize::SR_12_0 => [
                'US' => '13.5',
                'EUR' => '48.0',
                'UK' => '12.5',
            ],
            SkateLengthSize::SR_12_5 => [
                'US' => '14.0',
                'EUR' => '48.5',
                'UK' => '13.0',
            ],
            SkateLengthSize::SR_13_0 => [
                'US' => '14',
                'EUR' => '49',
                'UK' => '13.5',
            ],
            SkateLengthSize::SR_14_0 => [
                'US' => '15',
                'EUR' => '50',
                'UK' => '14.5',
            ],
            SkateLengthSize::SR_15_0 => [
                'US' => '16',
                'EUR' => '51',
                'UK' => '15.5',
            ],
        );
    }

    /**
     * Summary of getIntSkateLengthSizes
     * @return array<string, mixed>
     */
    public function getIntSkateLengthSizes(): array
    {
        return array(
            SkateLengthSize::INT_4_0 => [
                'US' => '5.0',
                'EUR' => '37.5',
                'UK' => '4.5',
            ],
            SkateLengthSize::INT_4_5 => [
                'US' => '5.5',
                'EUR' => '38',
                'UK' => '5.0',
            ],
            SkateLengthSize::INT_5_0 => [
                'US' => '6.0',
                'EUR' => '38.5',
                'UK' => '5.5',
            ],
            SkateLengthSize::INT_5_5 => [
                'US' => '6.5',
                'EUR' => '39',
                'UK' => '6.0',
            ],
            SkateLengthSize::INT_6_0 => [
                'US' => '7.5',
                'EUR' => '40.5',
                'UK' => '6.5',
            ],
            SkateLengthSize::INT_6_5 => [
                'US' => '8.0',
                'EUR' => '41.0',
                'UK' => '7.0',
            ],
        );
    }

    /**
     * Summary of getJrSkateLengthSizes
     * @return array<string, mixed>
     */
    public function getJrSkateLengthSizes(): array
    {
        return array(
            SkateLengthSize::JR_1_0 => [
                'US' => '2.0',
                'EUR' => '33.5',
                'UK' => '1.5',
            ],
            SkateLengthSize::JR_1_5 => [
                'US' => '2.5',
                'EUR' => '34',
                'UK' => '2.0',
            ],
            SkateLengthSize::JR_2_0 => [
                'US' => '3.0',
                'EUR' => '35',
                'UK' => '2.5',
            ],
            SkateLengthSize::JR_2_5 => [
                'US' => '3.5',
                'EUR' => '35.5',
                'UK' => '3.0',
            ],
            SkateLengthSize::JR_3_0 => [
                'US' => '4.0',
                'EUR' => '36',
                'UK' => '3.5',
            ],
            SkateLengthSize::JR_3_5 => [
                'US' => '4.5',
                'EUR' => '36.5',
                'UK' => '4.0',
            ],
        );
    }

    /**
     * Summary of getYthSkateLengthSizes
     * @return array<string, mixed>
     */
    public function getYthSkateLengthSizes(): array
    {
        return array(
            SkateLengthSize::YTH_6_0 => [
                'US' => '7.0',
                'EUR' => '23.5',
                'UK' => '6.5',
            ],
            SkateLengthSize::YTH_7_0 => [
                'US' => '8.0',
                'EUR' => '25',
                'UK' => '7.5',
            ],
            SkateLengthSize::YTH_7_5 => [
                'US' => '8.5',
                'EUR' => '25.5',
                'UK' => '8.0',
            ],
            SkateLengthSize::YTH_8_0 => [
                'US' => '9.0',
                'EUR' => '26',
                'UK' => '8.5',
            ],
            SkateLengthSize::YTH_8_5 => [
                'US' => '9.5',
                'EUR' => '26.5',
                'UK' => '9.0',
            ],
            SkateLengthSize::YTH_9_0 => [
                'US' => '10.0',
                'EUR' => '27',
                'UK' => '9.5',
            ],
            SkateLengthSize::YTH_9_5 => [
                'US' => '10.5',
                'EUR' => '27.5',
                'UK' => '10.0',
            ],
            SkateLengthSize::YTH_10_0 => [
                'US' => '11.0',
                'EUR' => '28',
                'UK' => '10.5',
            ],
            SkateLengthSize::YTH_10_5 => [
                'US' => '11.5',
                'EUR' => '28.5',
                'UK' => '11.0',
            ],
            SkateLengthSize::YTH_11_0 => [
                'US' => '12.0',
                'EUR' => '29.5',
                'UK' => '11.5',
            ],
            SkateLengthSize::YTH_11_5 => [
                'US' => '12.5',
                'EUR' => '30',
                'UK' => '12.0',
            ],
            SkateLengthSize::YTH_12_0 => [
                'US' => '13.0',
                'EUR' => '31',
                'UK' => '12.5',
            ],
            SkateLengthSize::YTH_12_5 => [
                'US' => '13.5',
                'EUR' => '31.5',
                'UK' => '13.0',
            ],
            SkateLengthSize::YTH_13_0 => [
                'US' => '1.0',
                'EUR' => '32',
                'UK' => '13.5',
            ],
            SkateLengthSize::YTH_13_5 => [
                'US' => '1.5',
                'EUR' => '33',
                'UK' => '1.0',
            ],
        );
    }
}
