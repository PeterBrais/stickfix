<?php

namespace App\Http\Controllers;

use App\Enums\DeliveryType;
use App\Http\Controllers\ReCaptcha3;
use App\Models\ShippingData;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

/**
 * Summary of ShippingDataController
 */
class ShippingDataController extends Controller
{
    /**
     * Summary of getShippingDataList
     * @return \Illuminate\Http\JsonResponse
     */
    public function getShippingDataList(): JsonResponse
    {
        try {
            $this->verifyAndAddShippingData();

            if (Auth::check()) {
                $shippingDataList = ShippingData::get();
            } else {
                if (Cache::has('getShippingDataListGuest')) {
                    $shippingDataList = Cache::get('getShippingDataListGuest');
                } else {
                    $shippingDataList = ShippingData::select(
                        'deliveryType',
                        'deliveryPriceInMinorUnit',
                        'freeDeliveryStartingPriceInMinorUnit'
                    )->where('isActive', true)->get();

                    Cache::put('getShippingDataListGuest', $shippingDataList, now()->addMinutes(10));
                }
            }

            $success = true;
            $message = null;
        } catch (\Exception $e) {
            $success = false;
            $message = 'error.errorSelectingData';
            $shippingDataList = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'shippingDataList' => $shippingDataList,
        ]);
    }

    /**
     * Summary of getShippingData
     * @param int $id
     * @return JsonResponse
     */
    public function getShippingData(int $id)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::SEE_SHIPPING_DATA)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        try {
            $shippingData = ShippingData::findOrFail($id);
            $success = true;
            $message = null;
        } catch (ModelNotFoundException $e) {
            $success = false;
            $message = 'error.recordNotFound';
            $shippingData = [];
        }

        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => null,
            'shippingData' => $shippingData,
        ]);
    }

    /**
     * Summary of storeOrEdit
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeOrEdit(Request $request): JsonResponse
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::ADD_OR_EDIT_SHIPPING_DATA)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        $rules = [
            'deliveryType' => 'required|string',
            'deliveryPrice' => 'required|numeric|min:0|max:2000000000',
            'freeDeliveryStartingPrice' => 'required|numeric|min:0|max:2000000000',
            'isActive' => 'boolean',
            'recaptcha' => 'required|string',
        ];

        $messages = [
            'deliveryType.required' => 'validation.requiredField',
            'deliveryType.string' => 'validation.requiredField',
            'deliveryPrice.required' => 'validation.requiredField',
            'deliveryPrice.numeric' => 'validation.mayContainOnlyNaturalNumbersOrNumbersWithDot',
            'deliveryPrice.min' => 'validation.mustBeAPositiveValue',
            'deliveryPrice.max' => 'validation.numberTooLarge',
            'freeDeliveryStartingPrice.required' => 'validation.requiredField',
            'freeDeliveryStartingPrice.numeric' => 'validation.mayContainOnlyNaturalNumbersOrNumbersWithDot',
            'freeDeliveryStartingPrice.min' => 'validation.mustBeAPositiveValue',
            'freeDeliveryStartingPrice.max' => 'validation.numberTooLarge',
            'isActive.boolean' => 'validation.requiredField',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails() || !ReCaptcha3::verify($request->recaptcha)) {
            return response()->json([
                'success' => false, 'message' => 'error.validationError', 'errors' => $validator->errors(),
            ]);
        }

        $deliveryType = $request->deliveryType;
        if (!HelperController::isValidDeliveryType($deliveryType)) {
            return response()->json([
                'success' => false,
                'message' => 'error.pleaseTryAgainLater',
                'errors' => [ 'deliveryType' => [ 'validation.requiredField' ] ] ,
            ]);
        }

        $isActive = $request->isActive;
        if (HelperController::isRestrictedDeliveryType($deliveryType)) {
            $isActive = true;
        }

        $deliveryPrice = $request->deliveryPrice;
        $freeDeliveryStartingPrice = $request->freeDeliveryStartingPrice;

        if ($request->deliveryType === DeliveryType::PICK_UP_IN_STORE_FOR_FREE) {
            $deliveryPrice = 0;
            $freeDeliveryStartingPrice = 0;
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');
        $returnMessage = 'recordAdded';

        /** @var ShippingData|null $shippingData */
        $shippingData = ShippingData::where('deliveryType', $deliveryType)->first();
        if ($shippingData) {
            $shippingData->deliveryPriceInMinorUnit = intval($deliveryPrice * 100);
            $shippingData->freeDeliveryStartingPriceInMinorUnit = intval($freeDeliveryStartingPrice * 100);
            $shippingData->editedAt = $dateTime->toDateTimeString();
            $shippingData->isActive = $isActive;
            $updated = $shippingData->save();

            if (!$updated) {
                return response()->json([
                    'success' => false, 'message' => 'error.databaseError', 'errors' => null
                ]);
            }

            $returnMessage = 'dataUpdated';
        } else {
            $shippingData = new ShippingData();
            $shippingData->deliveryType = $deliveryType;
            $shippingData->deliveryPriceInMinorUnit = intval($deliveryPrice * 100);
            $shippingData->freeDeliveryStartingPriceInMinorUnit = intval($freeDeliveryStartingPrice * 100);
            $shippingData->isActive = $isActive;
            $created = $shippingData->save();

            if (!$created) {
                return response()->json([
                    'success' => false, 'message' => 'error.databaseError', 'errors' => null
                ]);
            }
        }

        return response()->json([
            'success' => true, 'message' => $returnMessage, 'errors' => null
        ]);
    }

    /**
     * Summary of mark
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function mark(int $id)
    {
        if (!RolesController::checkIfAuthenticatedUserHasRole(RolesController::ADD_OR_EDIT_SHIPPING_DATA)) {
            return response()->json([
                'success' => false, 'message' => 'error.userHasNoPermission', 'errors' => null
            ]);
        }

        try {
            /** @var ShippingData $shippingData */
            $shippingData = ShippingData::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false, 'message' => 'error.recordNotFound', 'errors' => null
            ]);
        }

        if (HelperController::isRestrictedDeliveryType($shippingData->deliveryType)) {
            return response()->json([
                'success' => false, 'message' => 'error.forbiddenToEditEntry', 'errors' => null
            ]);
        }

        $shippingData->isActive = !$shippingData->isActive;
        $updated = $shippingData->save();

        if (!$updated) {
            return response()->json([
                'success' => false, 'message' => 'error.databaseError', 'errors' => null
            ]);
        }

        return response()->json([
            'success' => true, 'message' => 'dataUpdated', 'errors' => null
        ]);
    }

    /**
     * Summary of verifyAndAddShippingData
     * @return void
     */
    private function verifyAndAddShippingData(): void
    {
        try{
            $storedShippingDataList = ShippingData::select('deliveryType')->distinct()->get()->toArray();
            $storedDeliveryTypes = [];
            foreach ($storedShippingDataList as $shippingData) {
                $storedDeliveryTypes[] = $shippingData['deliveryType'];
            }

            foreach (DeliveryType::getValues() as $deliveryType) {
                $isDeliveryTypeExisting = in_array($deliveryType, $storedDeliveryTypes);
                if ($isDeliveryTypeExisting) {
                    continue;
                }

                $shippingData = new ShippingData();
                $shippingData->deliveryType = $deliveryType;
                $shippingData->isActive = false;

                if (HelperController::isRestrictedDeliveryType($shippingData->deliveryType)) {
                    $shippingData->isActive = true;
                }

                $shippingData->save();
            }
        } catch (\Exception $e) {

        }
    }
}
