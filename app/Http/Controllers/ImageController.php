<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Item;
use App\Models\Offer;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Summary of ImageController
 */
class ImageController extends Controller
{
    const ALLOWED_FILE_SIZE = 5000000; // 5MB

    const UPLOADS_FOLDER_NAME = 'uploads';
    const ITEMS_FOLDER_NAME = 'items';
    const OFFER_FOLDER_NAME = 'offers';
    const CAROUSEL_FOLDER_NAME = 'carousels';

    const MODEL_ITEM = 'item';
    const MODEL_ITEM_ID = 'itemId';

    const MODEL_OFFER = 'offer';
    const MODEL_OFFER_ID = 'offerId';

    const MODEL_CAROUSEL = 'carousel';

    /**
     * Summary of store
     * @param array<int, mixed> $images
     * @param string $path
     * @param Model $object
     * @return array<string, array<string>>
     */
    public static function store(array $images, string $path, Model $object): array
    {
        $returnKey = 'imageCollection';

        if ($object instanceof Offer) {
            $offerId = $object->id;
            $itemId = 0;
        } else if ($object instanceof Item) {
            $offerId = 0;
            $itemId = $object->id;
        } else {
            return [ $returnKey => [ 'Wrong object' ] ];
        }

        $dateTime = \Carbon\Carbon::now()->timezone('Europe/Riga');
        $errorList = [];

        foreach ($images as $key => $img) {
            if (!empty($img) && !empty($img['base64'])) {
                $result = self::prepareAndStoreImage($img['base64'], $path, $key, $dateTime->timestamp);
                $imageName = $result['imageName'];
                $receivedErrorList = $result['errorList'];

                if (is_array($receivedErrorList) && count($receivedErrorList) > 0) {
                    $errorList[$returnKey] = $receivedErrorList;
                    continue;
                }

                if (is_null($imageName)) {
                    $errorList[$returnKey][] = [ $key => 'validation.errorAddingImage' ];
                    continue;
                }

                $image = new Image();
                $image->itemId = $itemId;
                $image->offerId = $offerId;
                $image->name = $imageName;
                $image->path = self::getUploadPath($path);
                $created = $image->save();

                if (!$created) {
                    $errorList[$returnKey][] = [ $key => 'validation.errorAddingImage' ];
                    continue;
                }
            }
        }

        return $errorList;
    }

    /**
     * Summary of prepareAndStoreImage
     * @param string $base64
     * @param string $path
     * @param int $key
     * @param int|float|string $timestamp
     * @return array<string, mixed>
     */
    public static function prepareAndStoreImage(string $base64, string $path, int $key, mixed $timestamp): array
    {
        $errorList = [];

        try {
            $imageFile = self::getDecodedImage($base64);
            if ($imageFile === false) {
                throw new Exception();
            }
        } catch (Exception $e) {
            $errorList[] = [ $key => 'validation.errorAddingImage' ];
            return array(
                'imageName' => null,
                'errorList' => $errorList,
            );
        }

        $imageFile = strval($imageFile);

        try {
            $imageExt = self::getImageExtension($base64);
        } catch (Exception $e) {
            $errorList[] = [ $key => 'validation.errorAddingImage' ];
            return array(
                'imageName' => null,
                'errorList' => $errorList,
            );
        }

        if (!self::isValidImageExtension($imageExt)) {
            $errorList[] = [ $key => 'validation.incorrectImageFormatAllowed' ];
            return array(
                'imageName' => null,
                'errorList' => $errorList,
            );
        }

        if (self::getFileSize($imageFile) > self::getAllowedFileSize(self::ALLOWED_FILE_SIZE)) {
            $errorList[] = [ $key => 'validation.imageSizeMustBeLessThan5MB' ];
            return array(
                'imageName' => null,
                'errorList' => $errorList,
            );
        }

        $imageName = $key . '_' . $timestamp . '.' . $imageExt;
        Storage::disk(self::UPLOADS_FOLDER_NAME)->put($path . $imageName, $imageFile);

        return array(
            'imageName' => $imageName,
            'errorList' => $errorList,
        );
    }

    /**
     * Summary of deleteSelectedImages
     * @param array<int, mixed> $images
     * @param string $path
     * @return void
     */
    public static function deleteSelectedImages(array $images, string $path): void
    {
        foreach ($images as $img) {
            if (!empty($img) && !empty($img['id']) && !empty($img['path']) && !empty($img['name'])) {
                self::deleteSpecificImage($path . \strval($img['name']));
                Image::destroy(\intval($img['id']));
            }
        }
    }

    /**
     * Summary of deleteSpecificImage
     * @param string $path
     * @return void
     */
    public static function deleteSpecificImage(string $path): void
    {
        Storage::disk(self::UPLOADS_FOLDER_NAME)->delete($path);
    }

    /**
     * Summary of deleteAllModelImages
     * @param string $model
     * @param int $id
     * @return void
     */
    public static function deleteAllModelImages(string $model, int $id): void
    {
        if ($model === self::MODEL_ITEM) {
            self::deleteAllImages($id, self::MODEL_ITEM_ID);
            $folder = self::ITEMS_FOLDER_NAME;
        } else if ($model === self::MODEL_OFFER) {
            self::deleteAllImages($id, self::MODEL_ITEM_ID);
            $folder = self::OFFER_FOLDER_NAME;
        } else if ($model === self::MODEL_CAROUSEL) {
            $folder = self::CAROUSEL_FOLDER_NAME;
        } else {
            return;
        }

        Storage::disk(self::UPLOADS_FOLDER_NAME)->deleteDirectory($folder . '/' . $id);
    }

    /**
     * Summary of getDecodedImage
     * @param string $base64
     * @return string|false
     */
    public static function getDecodedImage(string $base64): string|false
    {
        $result = preg_replace('#^data:image/\w+;base64,#i', '', $base64);
        if (is_null($result)) {
            throw new Exception();
        }
        return base64_decode($result);
    }

    /**
     * Summary of getImageExtension
     * @param string $base64
     * @return string
     */
    public static function getImageExtension(string $base64): string
    {
        $type = mime_content_type($base64);
        if ($type === false) {
            throw new Exception();
        }
        return explode('/', strval($type))[1];
    }

    /**
     * Summary of isValidImageExtension
     * @param string $imageExt
     * @return bool
     */
    public static function isValidImageExtension(string $imageExt): bool
    {
        return in_array($imageExt, [ 'png', 'jpeg', 'jpg', 'bmp', 'webp' ]);
    }

    /**
     * Summary of getFileSize
     * @param string $imageFile
     * @return int
     */
    public static function getFileSize(string $imageFile): int
    {
        return strlen($imageFile);
    }

    // Encoded data is ~35% heavier
    /**
     * Summary of getAllowedFileSize
     * @param int $allowedSize
     * @return float
     */
    public static function getAllowedFileSize(int $allowedSize): float
    {
        return $allowedSize * 1.35;
    }

    /**
     * Summary of getObjectPath
     * @param string $type
     * @param int $key
     * @return string
     */
    public static function getObjectPath(string $type, int $key): string
    {
        return $type . '/' . $key . '/';
    }

    /**
     * Summary of getUploadPath
     * @param string $path
     * @return string
     */
    public static function getUploadPath(string $path): string
    {
        return './' . self::UPLOADS_FOLDER_NAME . '/' . $path;
    }

    /**
     * Summary of deleteAllImages
     * @param int $id
     * @param string $modelId
     * @return void
     */
    private static function deleteAllImages(int $id, string $modelId): void
    {
        $imageIds = [];
        $images = Image::where($modelId, $id)->get();

        foreach ($images as $img) {
            $imageIds[] = $img['id'];
        }

        if (count($imageIds)) {
            Image::destroy($imageIds);
        }
    }
}
