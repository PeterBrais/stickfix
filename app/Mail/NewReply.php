<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Summary of NewReply
 */
class NewReply extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Summary of data
     * @var mixed
     */
    public $data;
    /**
     * Summary of __construct
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Summary of build
     * @return NewReply
     */
    public function build()
    {
        return $this->view('emails.newReply')
            ->to($this->data['to'], $this->data['senderName'])
            ->subject('Stickfix.store - #' . $this->data['messageId'] . ' ' . $this->data['messageOption'] . '')
            ->from(config('mail.contact.address'), config('mail.contact.name'))
            ->with('data', $this->data);
    }
}
