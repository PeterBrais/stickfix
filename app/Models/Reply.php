<?php

namespace App\Models;

use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Summary of Reply
 */
class Reply extends Model
{
    use HasFactory;

    /**
     * Summary of timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Summary of fillable
     * @var array<int, string>
     */
    protected $fillable = [
        'messageId',
        'description',
        'isSent',
        'createdAt',
    ];

    /**
     * Summary of message
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo<\App\Models\Message, \App\Models\Reply>
     */
    public function message(): BelongsTo
    {
        return $this->belongsTo(Message::class, 'id');
    }
}
