<?php

namespace App\Models;

use App\Models\Image;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Item extends Model
{
    use HasFactory;
    use HasSlug;

    /**
     * Summary of timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Summary of fillable
     * @var array<int, string>
     */
    protected $fillable = [
        'userId',
        'title',
        'description',
        'priceInMinorUnit',
        'category',
        'isActive',
        'isDeleted',
        'state',
        'brand',
        'size',
        'stickSize',
        'stickFlex',
        'bladeCurve',
        'bladeSide',
        'skateSize',
        'skateLength',
        'skateWidth',
        'isAvailableForSale',
        'quantity',
        'gender',
        'isParcelDelivery',
        'createdAt',
        'editedAt',
    ];

    /**
     * Summary of author
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo<\App\Models\User, \App\Models\Item>
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'id');
    }

    /**
     * Summary of images
     * @return \Illuminate\Database\Eloquent\Relations\HasMany<\App\Models\Image>
     */
    public function images(): HasMany
    {
        return $this->hasMany(Image::class, 'itemId');
    }

    /**
     * Summary of getSlugOptions
     * @return \Spatie\Sluggable\SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Summary of getRouteKeyName
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
