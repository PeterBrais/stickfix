<?php

namespace App\Models;

use App\Models\Reply;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Summary of Message
 */
class Message extends Model
{
    use HasFactory;

    /**
     * Summary of timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Summary of fillable
     * @var array<int, string>
     */
    protected $fillable = [
        'option',
        'name',
        'email',
        'phoneNumber',
        'phoneCountry',
        'description',
        'clientLanguage',
        'isRead',
        'isAnswered',
        'isSentToExternalMail',
        'createdAt',
        'editedAt',
    ];

    /**
     * Summary of replies
     * @return \Illuminate\Database\Eloquent\Relations\HasMany<\App\Models\Reply>
     */
    public function replies(): HasMany
    {
        return $this->hasMany(Reply::class, 'messageId');
    }
}
