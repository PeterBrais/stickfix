<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    /**
     * Summary of timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Summary of fillable
     * @var array<int, string>
     */
    protected $fillable = [
        'stripeSessionId',
        'status',
        'productList',
        'productAmountInMinorUnit',
        'shippingAmountInMinorUnit',
        'totalAmountInMinorUnit',
        'clientFirstName',
        'clientLastName',
        'clientEmail',
        'clientPhoneCountry',
        'clientPhoneNumber',
        'clientLanguageCode',
        'deliveryType',
        'parcelMachineName',
        'clientCountryCode',
        'clientState',
        'clientCity',
        'clientAddress',
        'clientFullAddress',
        'clientZipCode',
        'isShownToClient',
        'isNotificationSent',
        'createdAt',
        'processedAt',
        'sentOutAt',
        'finishedAt',
        'lastOpenedAt',
    ];
}
