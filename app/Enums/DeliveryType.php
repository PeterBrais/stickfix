<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static COURIER()
 * @method static static OMNIVA()
 * @method static static PICK_UP_IN_STORE_FOR_FREE()
 *
 * @extends Enum<string>
 */
final class DeliveryType extends Enum
{
    const COURIER = 'COURIER';
    const OMNIVA = 'OMNIVA';
    const PICK_UP_IN_STORE_FOR_FREE = 'IN_STORE';
}
