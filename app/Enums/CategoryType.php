<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SKATES()
 * @method static static STICKS()
 * @method static static HELMET()
 * @method static static GLOVES()
 * @method static static PANTS()
 * @method static static SHOULDER_PADS()
 * @method static static SHIN_GUARDS()
 * @method static static ELBOW_PADS()
 * @method static static BAGS()
 * @method static static WEAR_AND_ACCESSORIES()
 * @method static static KITS()
 * @method static static KIDS_EQUIPMENT()
 * @method static static GOALIES_EQUIPMENT()
 * @method static static FIGURE_SKATING()
 * @method static static INLINE()
 * @method static static OTHER()
 *
 * @extends Enum<string>
 */
final class CategoryType extends Enum
{
    const SKATES = 'SKATES';
    const STICKS = 'STICKS';
    const HELMET = 'HELMET';
    const GLOVES = 'GLOVES';
    const PANTS = 'PANTS';
    const SHOULDER_PADS = 'SHOULDER_PADS';
    const SHIN_GUARDS = 'SHIN_GUARDS';
    const ELBOW_PADS = 'ELBOW_PADS';
    const BAGS = 'BAGS';
    const WEAR_AND_ACCESSORIES = 'WEAR_AND_ACCESSORIES';
    const KITS = 'KITS';
    const KIDS_EQUIPMENT = 'KIDS_EQUIPMENT';
    const GOALIES_EQUIPMENT = 'GOALIES_EQUIPMENT';
    const FIGURE_SKATING = 'FIGURE_SKATING';
    const INLINE = 'INLINE';
    const OTHER = 'OTHER';
}
