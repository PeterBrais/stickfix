<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static NONE()
 * @method static static BAUER()
 * @method static static CCM()
 * @method static static EASTON()
 * @method static static JOFA()
 * @method static static MISSION()
 * @method static static NIKE()
 * @method static static RBK()
 * @method static static TACKLA()
 * @method static static TRUE()
 * @method static static VAUGHN()
 * @method static static WARRIOR()
 * @method static static OTHER()
 *
 * @extends Enum<string>
 */
final class BrandType extends Enum
{
    const NONE = 'NONE';
    const BAUER = 'BAUER';
    const CCM = 'CCM';
    const EASTON = 'EASTON';
    const JOFA = 'JOFA';
    const MISSION = 'MISSION';
    const NIKE = 'NIKE';
    const RBK = 'RBK';
    const TACKLA = 'TACKLA';
    const TRUE = 'TRUE';
    const VAUGHN = 'VAUGHN';
    const WARRIOR = 'WARRIOR';
    const OTHER = 'OTHER';
}
