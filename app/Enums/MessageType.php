<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static APPLY_FOR_REPAIR()
 * @method static static APPLY_FOR_ITEM()
 * @method static static QUESTION()
 * @method static static OTHER()
 *
 * @extends Enum<string>
 */
final class MessageType extends Enum
{
    const APPLY_FOR_REPAIR = 'APPLY_FOR_REPAIR';
    const APPLY_FOR_ITEM = 'APPLY_FOR_ITEM';
    const QUESTION = 'QUESTION';
    const OTHER = 'OTHER';
}
