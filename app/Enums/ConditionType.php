<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static NEW()
 * @method static static USED()
 * @method static static SLIGHTLY_USED()
 *
 * @extends Enum<string>
 */
final class ConditionType extends Enum
{
    const NEW = 'NEW';
    const USED = 'USED';
    const SLIGHTLY_USED = 'SLIGHTLY_USED';
}
