<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static XL()
 * @method static static L()
 * @method static static M()
 * @method static static YTH()
 *
 * @extends Enum<string>
 */
final class KitSize extends Enum
{
    const XL = 'XL';
    const L = 'L';
    const M = 'M';
    const YTH = 'YTH';
}
