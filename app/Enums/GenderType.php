<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static MALE()
 * @method static static FEMALE()
 * @method static static UNISEX()
 *
 * @extends Enum<string>
 */
final class GenderType extends Enum
{
    const MALE = 'male';
    const FEMALE = 'female';
    const UNISEX = 'unisex';
}
