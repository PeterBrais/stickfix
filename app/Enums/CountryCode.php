<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LV()
 * @method static static LT()
 * @method static static EE()
 *
 * @extends Enum<string>
 */
final class CountryCode extends Enum
{
    const LV = 'LV';
    const LT = 'LT';
    const EE = 'EE';
}
