<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static LV()
 * @method static static EN()
 * @method static static RU()
 *
 * @extends Enum<string>
 */
final class Language extends Enum
{
    const LV = 'lv';
    const EN = 'en';
    const RU = 'ru';
}
