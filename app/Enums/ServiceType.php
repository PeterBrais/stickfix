<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OFFER()
 * @method static static BUY()
 *
 * @extends Enum<string>
 */
final class ServiceType extends Enum
{
    const OFFER = 'OFFER';
    const BUY = 'BUY';
}
