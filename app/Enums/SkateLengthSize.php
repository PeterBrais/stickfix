<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static JR_1_0()
 * @method static static JR_1_5()
 * @method static static JR_2_0()
 * @method static static JR_2_5()
 * @method static static JR_3_0()
 * @method static static JR_3_5()
 * @method static static INT_4_0()
 * @method static static INT_4_5()
 * @method static static INT_5_0()
 * @method static static INT_5_5()
 * @method static static INT_6_0()
 * @method static static INT_6_5()
 * @method static static SR_7_0()
 * @method static static SR_7_5()
 * @method static static SR_8_0()
 * @method static static SR_8_5()
 * @method static static SR_9_0()
 * @method static static SR_9_5()
 * @method static static SR_10_0()
 * @method static static SR_10_5()
 * @method static static SR_11_0()
 * @method static static SR_11_5()
 * @method static static SR_12_0()
 * @method static static SR_12_5()
 * @method static static SR_13_0()
 * @method static static SR_14_0()
 * @method static static SR_15_0()
 * @method static static YTH_6_0()
 * @method static static YTH_7_0()
 * @method static static YTH_7_5()
 * @method static static YTH_8_0()
 * @method static static YTH_8_5()
 * @method static static YTH_9_0()
 * @method static static YTH_9_5()
 * @method static static YTH_10_0()
 * @method static static YTH_10_5()
 * @method static static YTH_11_0()
 * @method static static YTH_11_5()
 * @method static static YTH_12_0()
 * @method static static YTH_12_5()
 * @method static static YTH_13_0()
 * @method static static YTH_13_5()
 *
 * @extends Enum<string>
 */
final class SkateLengthSize extends Enum
{
    const JR_1_0 = '1.0';
    const JR_1_5 = '1.5';
    const JR_2_0 = '2.0';
    const JR_2_5 = '2.5';
    const JR_3_0 = '3.0';
    const JR_3_5 = '3.5';
    const INT_4_0 = '4.0';
    const INT_4_5 = '4.5';
    const INT_5_0 = '5.0';
    const INT_5_5 = '5.5';
    const INT_6_0 = '6.0';
    const INT_6_5 = '6.5';
    const SR_7_0 = '7.0';
    const SR_7_5 = '7.5';
    const SR_8_0 = '8.0';
    const SR_8_5 = '8.5';
    const SR_9_0 = '9.0';
    const SR_9_5 = '9.5';
    const SR_10_0 = '10.0';
    const SR_10_5 = '10.5';
    const SR_11_0 = '11.0';
    const SR_11_5 = '11.5';
    const SR_12_0 = '12.0';
    const SR_12_5 = '12.5';
    const SR_13_0 = '13.0';
    const SR_14_0 = '14.0';
    const SR_15_0 = '15.0';
    const YTH_6_0 = '6.0';
    const YTH_7_0 = '7.0';
    const YTH_7_5 = '7.5';
    const YTH_8_0 = '8.0';
    const YTH_8_5 = '8.5';
    const YTH_9_0 = '9.0';
    const YTH_9_5 = '9.5';
    const YTH_10_0 = '10.0';
    const YTH_10_5 = '10.5';
    const YTH_11_0 = '11.0';
    const YTH_11_5 = '11.5';
    const YTH_12_0 = '12.0';
    const YTH_12_5 = '12.5';
    const YTH_13_0 = '13.0';
    const YTH_13_5 = '13.5';
}
