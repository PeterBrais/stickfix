<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SR_S()
 * @method static static SR_M()
 * @method static static SR_L()
 * @method static static SR_XL()
 * @method static static JR_S()
 * @method static static JR_M()
 * @method static static JR_L()
 * @method static static JR_XL()
 * @method static static YTH_S()
 * @method static static YTH_M()
 * @method static static YTH_L()
 * @method static static YTH_XL()
 *
 * @extends Enum<string>
 */
final class PadsAndPantsSize extends Enum
{
    // Sizes for - Pants, Shoulder pads, Elbow pads
    const SR_S = 'SR_S';
    const SR_M = 'SR_M';
    const SR_L = 'SR_L';
    const SR_XL = 'SR_XL';
    const JR_S = 'JR_S';
    const JR_M = 'JR_M';
    const JR_L = 'JR_L';
    const JR_XL = 'JR_XL';
    const YTH_S = 'YTH_S';
    const YTH_M = 'YTH_M';
    const YTH_L = 'YTH_L';
    const YTH_XL = 'YTH_XL';
}
