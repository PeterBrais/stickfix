import vue from '@vitejs/plugin-vue';
import laravel from 'laravel-vite-plugin';
import { defineConfig, loadEnv } from 'vite';

export default defineConfig(({command, mode}) => {
    const env = loadEnv(mode, process.cwd(), '')

    return{
        server: {
            hmr: {
                host: env.VITE_SERVER_URL,
            },
            port: env.VITE_SERVER_PORT,
        },
        plugins: [
            laravel({
                input: [
                    'resources/css/app.css',
                    'resources/js/app.js',
                ],
                refresh: ['resources/views/**'],
            }),
            vue({
                template: {
                    transformAssetUrls: {
                        base: null,
                        includeAbsolute: false,
                    },
                },
            }),
        ],
    }
});
