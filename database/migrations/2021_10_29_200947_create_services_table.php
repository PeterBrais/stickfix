<?php

use App\Enums\Language;
use App\Enums\ServiceType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->enum('serviceType', ServiceType::getValues());
            $table->string('title', 256);
            $table->string('note', 256)->nullable();
            $table->enum('language', Language::getValues())->nullable();
            $table->integer('priceInMinorUnit')->nullable();
            $table->boolean('isActive')->default(1);
            $table->timestamp('createdAt')->useCurrent();
            $table->timestamp('editedAt')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
