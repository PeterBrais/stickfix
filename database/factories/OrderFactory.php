<?php

namespace Database\Factories;

use App\Enums\DeliveryType;
use App\Enums\OrderStatus;
use App\Models\Order;
use Faker\Generator;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = app(Generator::class);

        $productAmountInMinorUnit = $faker->numberBetween(100, 2000000000);
        $shippingAmountInMinorUnit = $faker->numberBetween(100, 2000000000);

        return [
            'stripeSessionId' => $faker->text(512),
            'status' => OrderStatus::INCOMPLETE,
            'productList' => json_encode(
                [
                    [
                        'productId' => $faker->numberBetween(1, 4000000000),
                        'title' => $faker->sentence(4),
                        'priceInMinorUnit' => $faker->numberBetween(1, 200000000),
                        'quantity' => $faker->numberBetween(1, 10),
                        'quantityWithPriceInMinorUnit' => $faker->numberBetween(1, 10),
                    ],
                ],
            ),
            'productAmountInMinorUnit' => $productAmountInMinorUnit,
            'shippingAmountInMinorUnit' => $shippingAmountInMinorUnit,
            'totalAmountInMinorUnit' => $productAmountInMinorUnit + $shippingAmountInMinorUnit,
            'clientFirstName' => $faker->word(),
            'clientLastName' => $faker->word(),
            'clientEmail' => $faker->email(),
            'clientPhoneCountry' => $faker->countryCode(),
            'clientPhoneNumber' => $faker->phoneNumber(),
            'clientLanguageCode' => $faker->languageCode(),
            'deliveryType' => DeliveryType::OMNIVA,
            'parcelMachineName' => $faker->address(),
            'clientCountryCode' => $faker->countryCode(),
            'clientState' => null,
            'clientCity' => $faker->city(),
            'clientAddress' => $faker->address(),
            'clientFullAddress' => $faker->address(),
            'clientZipCode' => $faker->postcode(),
            'isShownToClient' => false,
            'isNotificationSent' => false,
            'createdAt' => now(),
            'processedAt' => null,
            'sentOutAt' => null,
            'finishedAt' => null,
            'lastOpenedAt' => null,
        ];
    }
}
