<?php

namespace Database\Factories;

use App\Enums\Language;
use App\Enums\MessageType;
use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;

/** @extends Factory<Message> */
class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var class-string<\App\Models\Message>
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'option' => MessageType::APPLY_FOR_REPAIR,
            'name' => 'Jon Doe',
            'email' => 'testmail@testmail.com',
            'phoneNumber' => '123456789',
            'phoneCountry' => 'EU',
            'description' => 'iminution expression reasonable',
            'clientLanguage' => Language::LV,
            'isRead' => 1,
            'isAnswered' => 1,
            'isSentToExternalMail' => 1,
            'createdAt' => now(),
            'editedAt' => now(),
        ];
    }
}
