<?php

namespace Database\Factories;

use App\Enums\DeliveryType;
use App\Models\ShippingData;
use Illuminate\Database\Eloquent\Factories\Factory;

/** @extends Factory<ShippingData> */
class ShippingDataFactory extends Factory
{
    /**
     * @var class-string<\App\Models\ShippingData>
     */
    protected $model = ShippingData::class;

    /**
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'deliveryType' => DeliveryType::PICK_UP_IN_STORE_FOR_FREE,
            'deliveryPriceInMinorUnit' => 100,
            'freeDeliveryStartingPriceInMinorUnit' => 1000,
            'isActive' => 1,
            'createdAt' => now(),
            'editedAt' => now(),
        ];
    }
}
