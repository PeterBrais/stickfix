<?php

namespace Database\Seeders;

use App\Enums\DeliveryType;
use App\Http\Controllers\RolesController;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Summary of DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Summary of run
     * @return void
     */
    public function run()
    {
        $adminId = DB::table('users')->insertGetId([
            'username' => 'admin',
            'email' => 'admin@stickfix.store',
            'password' => '$2y$10$Q2JFr4eh7VONly9pEbfBnusKUsmGpME/Y1lFP2nM0jTH2LsMY2Q76', // 12345678
            'isConfirmed' => true,
            'isShownToUser' => true,
        ]);

        DB::table('roles')->insert([
            'userId' => $adminId,
            'role' => RolesController::SUPER_ADMIN,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'OFFER',
            'title' => 'Hokeja nūjas remonts',
            'note' => 'Labojam TIKAI karbona nūjas',
            'priceInMinorUnit' => 2000,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'OFFER',
            'title' => 'Hokeja nūjas pagarināšana vai samazināšana',
            'note' => null,
            'priceInMinorUnit' => 1000,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'OFFER',
            'title' => 'Slidu asināšana',
            'note' => 'Asinām gan ar trapecveida, gan elipsveida asi!',
            'priceInMinorUnit' => 500,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'OFFER',
            'title' => 'Kapes izgatavošana',
            'note' => 'Kape pielāgota speciāli Jūsu zobiem!',
            'priceInMinorUnit' => 1000,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'BUY',
            'title' => 'Iepērkam salauztas vai bojātas karbona nūjas',
            'note' => null,
            'priceInMinorUnit' => 0,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'BUY',
            'title' => 'Iepērkam Jūsu veco vai nevajadzīgo hokeja aprīkojumu',
            'note' => null,
            'priceInMinorUnit' => 0,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'BUY',
            'title' => 'Iepērkam arī citas ar hokeju saistītas lietas',
            'note' => null,
            'priceInMinorUnit' => 0,
            'isActive' => true,
        ]);

        DB::table('services')->insert([
            'serviceType' => 'BUY',
            'title' => 'Iepērkam daiļslidošanas slidas',
            'note' => null,
            'priceInMinorUnit' => 0,
            'isActive' => true,
        ]);

        DB::table('offers')->insert([
            'title' => 'Hokeja nūju un aprīkojuma labošana',
            'description' => 'Piedāvājam hokeja nūju labošanu. Labošana norit ar karbonšķiedru materiāliem un tehnoloģijām. Piedāvājam hokeja aprīkojuma labošanu. Labojam cimdus, plecsargus un citas aprīkojuma daļas',
            'isActive' => true,
        ]);

        DB::table('offers')->insert([
            'title' => 'Pārdodam hokeja nūjas',
            'description' => 'Pārdodam gan lietotas, gan jaunas hokeja nūjas. Spēlētājiem un vārtsargiem uz abām spēlēšanas pusēm. Piedāvājam arī nūju pagarināšanas pakalpojumu',
            'isActive' => true,
        ]);

        DB::table('offers')->insert([
            'title' => 'Pārdodam hokeja aprīkojumu',
            'description' => 'Pārdodam lietotu un jaunu hokeja aprīkojumu dažādām vecuma grupām. Piedāvājam izveidot kapes',
            'isActive' => true,
        ]);

        $deliveryTypesWithAmounts = array(
            [
                'type' => DeliveryType::COURIER,
                'deliveryPriceInMinorUnit' => 500,
                'freeDeliveryStartingPriceInMinorUnit' => 10000,
            ],
            [
                'type' => DeliveryType::OMNIVA,
                'deliveryPriceInMinorUnit' => 500,
                'freeDeliveryStartingPriceInMinorUnit' => 10000,
            ],
            [
                'type' => DeliveryType::PICK_UP_IN_STORE_FOR_FREE,
                'deliveryPriceInMinorUnit' => 0,
                'freeDeliveryStartingPriceInMinorUnit' => 0,
            ],
        );

        foreach ($deliveryTypesWithAmounts as $deliveryOption) {
            DB::table('shipping_data')->insert([
                'deliveryType' => $deliveryOption['type'],
                'deliveryPriceInMinorUnit' => $deliveryOption['deliveryPriceInMinorUnit'],
                'freeDeliveryStartingPriceInMinorUnit' => $deliveryOption['freeDeliveryStartingPriceInMinorUnit'],
            ]);
        }
    }
}
