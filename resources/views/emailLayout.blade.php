<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name', 'StickFix') }}</title>
</head>
<body>
    <div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3 col-12">
                <div class="justify-content-center text-center py-5">
                    <img src="{{ $message->embed('uploads/default.png') }}" class="rounded" alt="StickFix" width="100" height="100">
                    <h2>Stickfix</h2>
                </div>

                @yield('content')

                <p>SIA Stickfix - <a href="{{ config('app.url') }}">{{ config('app.name_url') }}</a> </p>

                <ul class="list-unstyled mt-3">
                    <li><strong>Mūsu rekvizīti:</strong></li>
                    <li>{{ $data['requisites']['name'] }}</li>
                    <li>Reģ. Nr. {{ $data['requisites']['regNo'] }}</li>
                    <li>PVN Reģ. Nr. {{ $data['requisites']['pvnRegNo'] }}</li>
                    <li>Adrese: {{ $data['requisites']['address'] }}</li>
                    <li>E-Pasts: {{ $data['requisites']['email'] }}</li>
                    <li>Tālrunis: {{ $data['requisites']['phone'] }}</li>
                </ul>
			</div>
		</div>
	</div>
</body>
</html>
