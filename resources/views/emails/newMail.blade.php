@extends('emailLayout')

@section('content')

	<p><strong>Neatbildiet uz šo ziņojumu! Atbildiet tikai uz zemāk norādītajiem datiem</strong></p>

	<ul class="list-unstyled my-3">
		<li><strong>Dati:</strong></li>
		<li>Izvēle: {{ $data['messageOption'] }}</li>
		<li>Sūtītājs: {{ $data['senderName'] }}</li>
		<li>Epasts: {{ $data['senderEmail'] }}</li>
		<li>Telefona numurs: {{ $data['senderPhone'] }}</li>
	</ul>

	<hr>

	<p>Ziņas saturs:</p>
	{!! $data['messageDescription'] !!}

	<hr>

@endsection