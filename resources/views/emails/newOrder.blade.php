@extends('emailLayout')

@section('content')

	<p>Paldies, esam saņēmuši apmaksu par Jūsu pasūtījumu #{{ $data['orderId'] }}</p>

	<ul class="list-unstyled mt-3">
		<li><strong>Pasūtījums:</strong></li>
		<li>Pasūtījuma numurs: #{{ $data['orderId'] }}</li>
		<li>Pasūtījuma datums: {{ $data['orderDate'] }}</li>
		<li>Pasūtījuma statuss: {{ $data['orderStatus'] }}</li>
		<li>Saņēmejs: {{ $data['clientName'] }}</li>
		<li>E-Pasts: {{ $data['clientEmail'] }}</li>
		<li>Tālrunis: {{ $data['clientPhoneNumber'] }}</li>
		<li>Piegādes veids: {{ $data['deliveryMethod'] }}</li>
		<li>Piegādes adrese: {{ $data['deliveryAddress'] }}</li>
	</ul>

	<table class="table my-3">
		<thead>
			<tr>
				<th class="text-start">Preces nosaukums</th>
				<th class="text-start">Cena</th>
				<th class="text-start">Daudzums</th>
				<th class="text-start">Summa</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data['productList'] as $product)
				<tr>
					<td>
						{{ $product['titleWithSizes'] }}
					</td>
					<td>
						{{ $product['price'] }} EUR
					</td>
					<td>
						{{ $product['quantity'] }}
					</td>
					<td>
						{{ $product['quantityWithPrice'] }} EUR
					</td>
				</tr>
			@endforeach

			<tr>
				<td
					colspan="3"
					class="text-end"
				>
					Summa:
				</td>
				<td>
					{{ $data['productAmount'] }} EUR
				</td>
			</tr>

			<tr>
				<td
					colspan="3"
					class="text-end"
				>
					Piegāde:
				</td>
				<td>
					{{ $data['shippingAmount'] }} EUR
				</td>
			</tr>

			<tr>
				<td
					colspan="3"
					class="text-end"
				>
					<strong>Kopā:</strong>
				</td>
				<td>
					{{ $data['totalAmount'] }} EUR
				</td>
			</tr>
		</tbody>
	</table>

@endsection