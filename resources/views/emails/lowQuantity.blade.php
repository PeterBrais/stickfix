@extends('emailLayout')

@section('content')

	<p>Preces <a href="{{ config('app.url') }}/item/{{ $data['itemId'] }}">{{ $data['itemTitle'] }}</a> daudzums ir samazinājies līdz 0</p>

	@if($data['isImageAvailable'])
		<img src="{{ $message->embed($data['imageUrl']) }}" class="rounded" alt="Item" width="150" height="150">
	@endif

	<ul class="list-unstyled my-3">
		<li>Preces ID: {{ $data['itemId'] }}</li>
		<li>Nosaukums: {{ $data['itemTitle'] }}</li>
		<li>Cena: {{ $data['itemPrice'] }} EUR</li>
	</ul>

@endsection