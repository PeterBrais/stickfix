@extends('emailLayout')

@section('content')

	<p>Saņemts jauns pasūtījums <a href="{{ config('app.url') }}/order/{{ $data['orderId'] }}">#{{ $data['orderId'] }}</a> Summa: {{ $data['totalAmount'] }} EUR</p>

@endsection