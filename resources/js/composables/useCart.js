import { computed } from 'vue';
import { useStore } from 'vuex';
import { $t } from "../locales/localization";
import useMessage from './useMessage';

export default function useCart() {
    const store = useStore();
    const { setErrorMessage } = useMessage();

    const cartContent = computed(() => {
        return store.getters['cart/cart'];
    });

    function increaseQuantityOfItem(itemId, itemQuantity) {
        if (itemQuantityInCart(itemId) >= itemQuantity) {
            setErrorMessage($t('error.exceedsAvailableQuantity'));
            return;
        }

        store.dispatch('cart/increaseQuantityOfItemOrAddItemToCart', {
            itemId: itemId,
            quantity: 1,
        });
    };

    function decreaseQuantityOfItem(itemId) {
        store.dispatch('cart/decreaseQuantityOfItemOrRemoveItemFromCart', {
            itemId: itemId,
            quantity: 1,
        });
    };

    function removeItemsFromCart(itemList) {
        const itemIdList = [];
        itemList.forEach((item) => {
            itemIdList.push({ id: item.id });
        })

        store.dispatch('cart/removeItemsFromCart', {
            items: itemIdList,
        });
    };

    function itemQuantityInCart(itemId) {
        if (!Array.isArray(cartContent.value) || cartContent.value.length === 0) {
            return 0;
        }

        const productIndex = cartContent.value.findIndex((item) => {
            if (!item.id) {
                return -1;
            }
            return item.id === itemId
        });
        if (productIndex === -1) {
            return 0;
        }
        return cartContent.value[productIndex].quantity;
    };

    function emptyTheCart() {
        store.dispatch('cart/emptyTheCart');
    };

    return {
        increaseQuantityOfItem,
        decreaseQuantityOfItem,
        removeItemsFromCart,
        itemQuantityInCart,
        emptyTheCart,
    };
}