import { reactive } from 'vue';

export default function useFormData(initialValues) {
    const formValues = reactive(initialValues);

    return { formValues };
}
