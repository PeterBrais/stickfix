import { reactive } from 'vue';
import { $t } from "../locales/localization";

export default function useImage(initialImageFieldValues, formValues, errorValues = null) {
    const imageFields = reactive(initialImageFieldValues);

    function createSingleImage() {
        if (!formValues[imageFields.new].data) {
            return;
        }

        uploadImage(formValues[imageFields.new]);
    };

    function deleteSingleImage() {
        formValues[imageFields.new] = {
            data: null,
            base64: '',
            preview: ''
        }
    };

    function addImage() {
        formValues[imageFields.new].push( { data: null, base64: '', preview: '' } );
    };

    function createImage(index) {
        if (formValues[imageFields.new].length === 0
            || !formValues[imageFields.new][index]
            || !formValues[imageFields.new][index].data
        ) {
            return;
        }

        uploadImage(formValues[imageFields.new][index]);
    };

    function uploadImage(formField) {
        const reader = new FileReader();
        reader.onload = (e) => {
            formField.base64 = e.target.result;
        };
        reader.readAsDataURL(formField.data[0]);
        formField.preview = URL.createObjectURL(formField.data[0]);
    };

    function deleteImage(index) {
        if (formValues[imageFields.new].length === 0 || !formValues[imageFields.new][index]) {
            return;
        }
        formValues[imageFields.new].splice(index, 1);
    };

    function removeEmptyImages() {
        formValues[imageFields.new] = formValues[imageFields.new].filter(image => {
            return image.base64 && image.base64 !== '' && image.base64 !== undefined
        });
    };

    function addToDeletedImageList(index) {
        if (formValues[imageFields.stored].length === 0 || !formValues[imageFields.stored][index]) {
            return;
        }
        const image = formValues[imageFields.stored][index];
        formValues[imageFields.deleted].push( { id: image.id, path: image.path, name: image.name } );
        formValues[imageFields.stored].splice(index, 1);
    };

    function addToActiveImageList(index) {
        if (formValues[imageFields.deleted].length === 0 || !formValues[imageFields.deleted][index]) {
            return;
        }
        const image = formValues[imageFields.deleted][index];
        formValues[imageFields.stored].push( { id: image.id, path: image.path, name: image.name } );
        formValues[imageFields.deleted].splice(index, 1);
    };

    function validateImage(item, index) {
        if (errorValues === null) {
            return [];
        }

        const errors = [];
        if (errorValues[imageFields.error]
            && errorValues[imageFields.error].length > 0
            && errorValues[imageFields.error][index]
        ) {
            errors.push($t(errorValues[imageFields.error][index][0]));
            errorValues[imageFields.error][index] = null;
        }

        if (!item.$dirty) {
            return errors;
        }
        item.$errors.forEach((error) => {
            errors.push(error.$message);
        });
        return errors;
    };

    return {
        createSingleImage,
        deleteSingleImage,
        addImage,
        createImage,
        deleteImage,
        removeEmptyImages,
        addToDeletedImageList,
        addToActiveImageList,
        validateImage,
    };
}
