import { helpers } from '@vuelidate/validators';
import { $t } from "../../locales/localization";

export default function useItemRules() {
    function itemLength(value) {
        return value && value.length > 0;
    }

    const itemRule = helpers.withMessage($t('validation.mustSelectAtLeastOneItem'), itemLength);

    return { itemRule };
}
