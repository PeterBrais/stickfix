import { helpers, required, requiredIf } from '@vuelidate/validators';
import { $t } from "../../locales/localization";

export default function useImageRules() {
    function fileSize(value) {
        if (value === null || value === undefined || value === '') {
            return true;
        }
        return (value[0].size < 5000000);
    }

    function requiredIfRule(field) {
        return helpers.withMessage($t('validation.requiredField', { num: '5' }), requiredIf(function() { return field}));
    }

    const requiredRule = helpers.withMessage($t('validation.requiredField'), required);

    const fileSizeRule = helpers.withMessage($t('validation.imageSizeMustBeLessThan', { num: '5' }), fileSize);

    const imageRules = {
        data: {
            fileSize: fileSizeRule
        },
        base64: { },
        preview: { },
    };

    function additionalImageRules(additionalRules = null) {
        return {
            data: {
                ...additionalRules,
                fileSize: fileSizeRule,
            },
            base64: { },
            preview: { },
        }
    };

    return { requiredIfRule, requiredRule, imageRules, additionalImageRules };
}
