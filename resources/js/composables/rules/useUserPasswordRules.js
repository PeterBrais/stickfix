import { helpers } from '@vuelidate/validators';
import { computed, ref } from 'vue';
import { useStore } from 'vuex';
import { $t } from "../../locales/localization";
import useMessage from '../useMessage';
const { withAsync } = helpers;

export default function useUserPasswordRules(formValues, isLoading) {
    const store = useStore();

    const userData = computed(() => { return store.getters['auth/user']; });
    const passwordAvailabilityTimer = ref(null);
    const { setErrorMessage } = useMessage();

    const isPasswordCorrectRule = helpers.withMessage($t('validation.passwordIsIncorrect'), withAsync(isPasswordCorrectPromise));

    async function isPasswordCorrect(userId, value) {
        const formData = {
            id: userId,
            currentPassword: value,
        };

        const response = await axios.post('/api/checkPasswordMatch', formData)
            .then((response) => {
                return response.data.match;
            }).catch((error) => {
                console.error(error);
                formValues.recaptcha = '';
                isLoading.value = true;
                setErrorMessage($t('error.pleaseTryAgainLater'));
                return false;
            });

        return Boolean(await response);
    };

    function isPasswordCorrectPromise(value) {
        if (isLoading.value) {
            return false;
        }

        if (value === '') {
            return true;
        }

        if (!userData.value || !userData.value.id) {
            console.error('User undefined');
            formValues.recaptcha = '';
            isLoading.value = true;
            setErrorMessage($t('error.userNotFound'));
            return false;
        }

        return new Promise((resolve) => {
            if (passwordAvailabilityTimer.value) {
                clearTimeout(passwordAvailabilityTimer.value)
                passwordAvailabilityTimer.value = null
            }
            passwordAvailabilityTimer.value = setTimeout(() => {
                resolve(isPasswordCorrect(userData.value.id, value));
            }, 500);
        });
    };

    return { isPasswordCorrectRule };
}
