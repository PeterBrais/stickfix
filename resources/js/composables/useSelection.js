import { $t } from "../locales/localization";

export default function useSelection() {
    const notSpecified = '-';

    function getListWithNotSpecified(items) {
        return [notSpecified].concat(items);
    };

    function getSelectionItems(items, location = '') {
        const selectionList = [];

        items.forEach(item => {
            selectionList.push({
                text: $t('enums.' + location + item),
                value: item,
            });
        });

        return selectionList;
    };

    function getSelectionItemsWithoutEnums(items) {
        const selectionList = [];

        items.forEach(item => {
            selectionList.push({
                text: item,
                value: item,
            });
        });

        return selectionList;
    };

    function prepareTableItems(items) {
        items.forEach((item) => {
            item.text = this.$t('enums.' + item.value);
        });

        return items;
    };

    return {
        getListWithNotSpecified,
        getSelectionItems,
        getSelectionItemsWithoutEnums,
        prepareTableItems,
    };
}