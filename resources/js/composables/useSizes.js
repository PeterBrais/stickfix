import { computed } from 'vue';
import { useStore } from 'vuex';
import { $t } from "../locales/localization";

export default function useSizes(formValues = null) {
    const store = useStore();

    const sizesEnum = computed(() => {
        return store.getters['enums/sizes'];
    });

    function getSizes(category) {
        if (!category) {
            return [];
        }

        if (category === 'HELMET') {
            return sizesEnum.value.HELMET;
        } else if (category === 'GLOVES') {
            return sizesEnum.value.GLOVES;
        } else if (category === 'PANTS'
            || category === 'SHOULDER_PADS'
            || category === 'ELBOW_PADS'
        ) {
            return sizesEnum.value.PANTS_SHOULDER_ELBOW_PAD_SIZES;
        } else if (category === 'SHIN_GUARDS') {
            return sizesEnum.value.SHIN_GUARD;
        } else if (category === 'BAGS') {
            return sizesEnum.value.BAG;
        } else if (category === 'KITS') {
            return sizesEnum.value.KIT;
        } else {
            return [];
        }
    };

    function getSkateLengths(skateSize) {
        const skateSizes = [];

        if (!skateSize) {
            return skateSizes;
        }

        Object.entries(sizesEnum.value.SKATES.LENGTH[skateSize]).forEach(entry => {
            const [key, value] = entry;

            skateSizes.push({
                text: key + ' - EUR ' + value.EUR,
                value: key,
            });
        });

        return skateSizes;
    };

    function getSkateLengthsWithSizes(skateSizes) {
        const preparedSkateSizes = [];

        if (!skateSizes || !skateSizes.length) {
            return preparedSkateSizes;
        }

        skateSizes.forEach(skateSize => {
            Object.entries(sizesEnum.value.SKATES.LENGTH[skateSize]).forEach(entry => {
                const [key, value] = entry;
                preparedSkateSizes.push({
                    text: $t('enums.' + skateSize) + ' ' + key + ' (EUR ' + value.EUR + ')',
                    value: key + ' - EUR ' + value.EUR,
                });
            });
        })

        return preparedSkateSizes;
    };

    function resetSizes() {
        if (formValues === null || formValues.size === undefined) {
            return;
        }
        formValues.size = '';
    };

    function resetSkateLength() {
        if (formValues === null || formValues.skateLength === undefined) {
            return;
        }
        formValues.skateLength = '';
    };

    function getItemTitleWithSize(item) {
        if (!item) {
            return '';
        }

        let titleWithSizes = item.title;
        if (item.category === 'STICKS') {
            const stickFlex = item.stickFlex ? item.stickFlex + 'FLEX, ' : '';
            const stickSize = item.stickSize
                ? $t('enums.' + item.stickSize) + ', '
                : '';
            const bladeCurve = item.bladeCurve ? item.bladeCurve + ', ' : '';
            const bladeSide = item.bladeSide
                ? $t('enums.' + item.bladeSide)
                : '';
            const preparedSizesTitle = (stickFlex + stickSize + bladeCurve + bladeSide).trim();
            titleWithSizes += preparedSizesTitle.length ? ', ' + preparedSizesTitle  : '';
        } else if (item.category === 'SKATES') {
            const skateSize = item.skateSize
                ? $t('enums.' + item.skateSize) + ', '
                : '';
            const skateLength = item.skateLength ? item.skateLength + ', ' : '';
            const skateWidth = item.skateWidth ? item.skateWidth : '';
            const preparedSizesTitle = (skateSize + skateLength + skateWidth).trim();
            titleWithSizes += preparedSizesTitle.length ? ', ' + preparedSizesTitle  : '';
        } else {
            titleWithSizes += item.size
                ? ', ' + $t('enums.' + item.size)
                : '';
        }

        return titleWithSizes;
    };

    return {
        getSizes,
        getSkateLengths,
        getSkateLengthsWithSizes,
        resetSizes,
        resetSkateLength,
        getItemTitleWithSize
    };
}