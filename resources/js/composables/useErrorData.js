import { reactive } from 'vue';

export default function useErrorData(initialValues) {
    const errorValues = reactive(initialValues);

    return { errorValues };
}
