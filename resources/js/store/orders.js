export default {
    namespaced: true,

    state: {
        orderList: null,
        newOrderCount: null,
        isLoading: true,
    },

    getters: {
        orderList (state) {
            return state.orderList
        },
        newOrderCount (state) {
            return state.newOrderCount
        },
        isLoading (state) {
            return state.isLoading
        },
    },

    mutations: {
        SET_ORDER_LIST (state, value) {
            state.orderList = value
        },
        SET_NEW_ORDER_COUNT (state, value) {
            state.newOrderCount = value
        },
        SET_IS_LOADING (state, value) {
            state.isLoading = value
        },
    },

    actions: {
        async getOrders ({ dispatch }) {
            return dispatch('getOrderList')
        },

        getOrderList ({ commit }) {
            commit('SET_IS_LOADING', true);
            return axios.get('/api/checkAuth').then(response => {
                if (response.data.success) {
                    axios.get('/api/getOrders').then(response => {
                        if (response.data.success) {
                            commit('SET_ORDER_LIST', response.data.orders);
                            commit(
                                'SET_NEW_ORDER_COUNT',
                                response.data.orders.filter(order => order.lastOpenedAt === null).length
                            );
                            commit('SET_IS_LOADING', false);
                        } else {
                            commit('SET_ORDER_LIST', []);
                            commit('SET_NEW_ORDER_COUNT', 0);
                            commit('SET_IS_LOADING', true);
                        }
                    }).catch((error) => {
                        console.error(error);
                        commit('SET_ORDER_LIST', []);
                        commit('SET_NEW_ORDER_COUNT', 0);
                        commit('SET_IS_LOADING', true);
                    });
                } else {
                    commit('SET_ORDER_LIST', []);
                    commit('SET_NEW_ORDER_COUNT', 0);
                    commit('SET_IS_LOADING', true);
                }
            });
        }
    }
}
