export default {
    namespaced: true,

    state: {
        shippingDataList: null,
        activeShippingDataList: null,
        isLoading: true,
    },

    getters: {
        shippingDataList (state) {
            return state.shippingDataList;
        },
        activeShippingDataList (state) {
            return state.activeShippingDataList;
        },
        isLoading (state) {
            return state.isLoading;
        },
    },

    mutations: {
        SET_SHIPPING_DATA_LIST (state, value) {
            state.shippingDataList = value;
        },
        SET_ACTIVE_SHIPPING_DATA_LIST (state, value) {
            state.activeShippingDataList = value;
        },
        SET_IS_LOADING (state, value) {
            state.isLoading = value;
        },
    },

    actions: {
        async getShippingData ({ dispatch }) {
            return dispatch('getShippingDataList');
        },

        getShippingDataList ({ commit }) {
            commit('SET_IS_LOADING', true);
            return axios.get('/api/getShippingDataList').then((response) => {
                if (response.data.success) {
                    commit('SET_SHIPPING_DATA_LIST', response.data.shippingDataList);
                    commit(
                        'SET_ACTIVE_SHIPPING_DATA_LIST',
                        response.data.shippingDataList.filter((shippingData) => {
                            if (shippingData.isActive === undefined) {
                                return true;
                            }
                            return shippingData.isActive;
                        })
                    );
                    commit('SET_IS_LOADING', false);
                } else {
                    commit('SET_SHIPPING_DATA_LIST', []);
                    commit('SET_ACTIVE_SHIPPING_DATA_LIST', []);
                    commit('SET_IS_LOADING', true);
                }
            }).catch((error) => {
                console.error(error);
                commit('SET_SHIPPING_DATA_LIST', []);
                commit('SET_ACTIVE_SHIPPING_DATA_LIST', []);
                commit('SET_IS_LOADING', true);
            });
        }
    }
}
