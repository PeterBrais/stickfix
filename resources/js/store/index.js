import { createStore } from 'vuex';
import auth from './auth';
import carousel from './carousel';
import cart from './cart';
import customer from './customer';
import enums from './enums';
import items from './items';
import message from './message';
import messages from './messages';
import offers from './offers';
import orders from './orders';
import parcels from './parcels';
import services from './services';
import shippingData from './shippingData';
import users from './users';

export const store = createStore({
    modules: {
        auth,
        items,
        services,
        offers,
        enums,
        message,
        users,
        messages,
        carousel,
        cart,
        parcels,
        customer,
        orders,
        shippingData,
    },
});
